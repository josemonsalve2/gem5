/**
 * @file spike_wrapper.hh
 * @author Jose M Monsalve Diaz (jmonsalvediaz@anl.gov)
 * @brief Glue logic to connect to the spike sim without a terminal
 * @version 0.1
 * @date 2021-06-28
 *
 * @copyright Copyright (c) 2021
 *
 * The spike simulator is available through a terminal CLI app.
 * If we want to connect it to Gem5 we need to unwrap this app
 * to allow access to the underlying logic.
 *
 *
 */

#include "spike_wrapper.hh"

#include <dlfcn.h>
#include <fesvr/option_parser.h>

#include "riscv/cachesim.h"
#include "extension.h"
#include "mmu.h"
#include "sim.h"
#include "dts.h"
#include "debug_module.h"
#include "riscv/debug_defines.h"

extern "C" {
 bool signal_exit = false;
}

namespace spike
{
    class spike_sim;

    class wrapper_sim_t : public htif_t, public simif_t
    {
        private:
            uint64_t current_step;
            bool cont;
            pthread_t sim_thread;
            spike_sim *parent_sim;

            // Separating gem5 and spike contexts
            context_t *gem5_ctx;
            context_t spike_ctx;
            size_t steps_n;
            bool blocked;

        protected:
            std::vector<std::pair<reg_t, mem_t*>> mems;
            std::vector<std::pair<reg_t, abstract_device_t*>> plugin_devices;
            mmu_t* debug_mmu;  // debug port into main memory
            std::vector<processor_t*> procs;
            reg_t initrd_start;
            reg_t initrd_end;
            const char* bootargs;
            reg_t start_pc;
            std::string dts;
            std::string dtb;
            std::unique_ptr<rom_device_t> boot_rom;

            bus_t bus;

            std::ostream sout_; // used for socket and terminal interface

            const char* get_dts() { if (dts.empty()) reset(); return dts.c_str(); }
            processor_t* get_core(const std::string& i);
            static const size_t INSNS_PER_RTC_TICK = 100; // 10 MHz clock for 1 BIPS core
            static const size_t CPU_HZ = 1000000000; // 1GHz CPU

            // memory-mapped I/O routines
            char* addr_to_mem(reg_t addr);
            bool mmio_load(reg_t addr, size_t len, uint8_t* bytes, bool atomic);
            bool mmio_store(reg_t addr, size_t len, const uint8_t* bytes, bool atomic);
            void make_dtb();
            void set_rom();

            const char* get_symbol(uint64_t addr);

            friend class processor_t;
            friend class mmu_t;
            friend class spike_sim;

            void reset();
            void idle();
            void read_chunk(addr_t taddr, size_t len, void* dst);
            void write_chunk(addr_t taddr, size_t len, const void* src);
            size_t chunk_align() { return 8; }
            size_t chunk_max_size() { return 8; }
            void set_target_endianness(memif_endianness_t endianness);
            memif_endianness_t get_target_endianness() const;

            // HTIF
            std::function<void(reg_t)> fromhost_callback;
            std::queue<reg_t> fromhost_queue;

        public:
        void set_steps_n(size_t n) { steps_n = n; }
        void set_blocked(bool blked) { blocked = blked; }
        void continue_spike_sim() {
            spike_ctx.switch_to();
        }
        void wait_for_mem() {
            gem5_ctx->switch_to();
        }
        void step(); // step through simulation

        uint64_t getPC() {
            return this->procs[0]->get_state()->pc;
        }
        
        std::string disasm_cur_inst() {
            // TODO: This requires accessing the instruction directly,
            // but the method below is accessing it through the simulation
            // fetch, which throws an exception. This needs to be
            // changed to directly access memory, bypassing simulator

            // auto disasm = procs[0]->get_disassembler();
            // auto pc = procs[0]->get_state()->pc;
            // auto ic_entry = procs[0]->get_mmu()->access_icache(pc);
            // auto inst = ic_entry->data;
            // if(!procs[0]->get_state()->serialized)
            //     return disasm->disassemble(inst.insn);
            // else
            return std::string();
        }

        wrapper_sim_t(const char* isa, const char* priv,
            const char* varch, size_t nprocs,
            bool halted, reg_t initrd_start, reg_t initrd_end,
            const char* bootargs, reg_t start_pc,
            std::vector<std::pair<reg_t, mem_t*>> mems,
            std::vector<std::pair<reg_t, abstract_device_t*>> plugin_devices,
            const std::vector<std::string>& args, 
            const std::vector<int> hartids, spike_sim* parent);

        processor_t* get_core(size_t i) { return procs.at(i); }; 
        unsigned nprocs() const { return procs.size(); }
    
        // Callback for processors to let the simulation know they were reset.
        void proc_reset(unsigned id);

        void htif_step() {
            if (tohost_addr == 0)
                return;

            if (auto tohost = from_target(memif().read_uint64(tohost_addr))) {
            memif().write_uint64(tohost_addr, target_endian<uint64_t>::zero);
            command_t cmd(memif(), tohost, fromhost_callback);
            device_list.handle_command(cmd);
            } else {
            idle();
            }

            // This is to gain an extra 30% execution time.
            // Will this be ever needed for Spike+Gem5
            device_list.tick();

            if (!fromhost_queue.empty() && !memif().read_uint64(fromhost_addr)) {
            memif().write_uint64(fromhost_addr, to_target(fromhost_queue.front()));
            fromhost_queue.pop();
            }
        }

        int run() {
            htif_t::start();
            // Helper function for spike_sim context
            auto fnc = [](void * sim) {
                auto sim_ptr = reinterpret_cast<wrapper_sim_t*>(sim);
                sim_ptr->step();
            };
            gem5_ctx = context_t::current();
            spike_ctx.init(fnc, this);
            auto enq_func = [](std::queue<reg_t>* q, uint64_t x) { q->push(x); };
            fromhost_callback = std::bind(enq_func, &fromhost_queue, std::placeholders::_1);

            return 0;
        }

        ~wrapper_sim_t() {
            stop();
            for (size_t i = 0; i < procs.size(); i++)
                delete procs[i];
            delete debug_mmu;
        }
    };

    void wrapper_sim_t::make_dtb()
    {
        dts = make_dts(INSNS_PER_RTC_TICK, CPU_HZ, initrd_start, initrd_end, bootargs, procs, mems);
        dtb = dts_compile(dts);
    }

    void wrapper_sim_t::set_rom()
    {
        const int reset_vec_size = 8;

        start_pc = start_pc == reg_t(-1) ? get_entry_point() : start_pc;

        uint32_t reset_vec[reset_vec_size] = {
            0x297,                                      // auipc  t0,0x0
            0x28593 + (reset_vec_size * 4 << 20),       // addi   a1, t0, &dtb
            0xf1402573,                                 // csrr   a0, mhartid
            get_core(0)->get_xlen() == 32 ?
            0x0182a283u :                             // lw     t0,24(t0)
            0x0182b283u,                              // ld     t0,24(t0)
            0x28067,                                    // jr     t0
            0,
            (uint32_t) (start_pc & 0xffffffff),
            (uint32_t) (start_pc >> 32)
        };
        if (get_target_endianness() == memif_endianness_big) {
            int i;
            // Instuctions are little endian
            for (i = 0; reset_vec[i] != 0; i++)
            reset_vec[i] = to_le(reset_vec[i]);
            // Data is big endian
            for (; i < reset_vec_size; i++)
            reset_vec[i] = to_be(reset_vec[i]);

            // Correct the high/low order of 64-bit start PC
            if (get_core(0)->get_xlen() != 32)
            std::swap(reset_vec[reset_vec_size-2], reset_vec[reset_vec_size-1]);
        } else {
            for (int i = 0; i < reset_vec_size; i++)
            reset_vec[i] = to_le(reset_vec[i]);
        }

        std::vector<char> rom((char*)reset_vec, (char*)reset_vec + sizeof(reset_vec));

        std::string dtb;
        dts = make_dts(INSNS_PER_RTC_TICK, CPU_HZ, initrd_start, initrd_end, bootargs, procs, mems);
        dtb = dts_compile(dts);

        rom.insert(rom.end(), dtb.begin(), dtb.end());
        const int align = 0x1000;
        rom.resize((rom.size() + align - 1) / align * align);

        boot_rom.reset(new rom_device_t(rom));
        bus.add_device(DEFAULT_RSTVEC, boot_rom.get());
    }

    static bool paddr_ok(reg_t addr)
    {
        return (addr >> MAX_PADDR_BITS) == 0;
    }

    bool
    wrapper_sim_t::mmio_load(reg_t addr, size_t len, uint8_t* bytes, bool atomic)
    {
    if (addr + len < addr || !paddr_ok(addr + len - 1))
        return false;
    return bus.load(addr, len, bytes, atomic);
    }

    bool
    wrapper_sim_t::mmio_store(reg_t addr, size_t len, const uint8_t* bytes, bool atomic)
    {
    if (addr + len < addr || !paddr_ok(addr + len - 1))
        return false;
    return bus.store(addr, len, bytes, atomic);
    }

    char*
    wrapper_sim_t::addr_to_mem(reg_t addr) {
    if (!paddr_ok(addr))
        return NULL;
    auto desc = bus.find_device(addr);
    if (auto mem = dynamic_cast<mem_t*>(desc.second))
        if (addr - desc.first < mem->size())
        return mem->contents(addr - desc.first);
    return NULL;
    }

    const char*
    wrapper_sim_t::get_symbol(uint64_t addr)
    {
    return htif_t::get_symbol(addr);
    }

    // htif
    void wrapper_sim_t::reset()
    { 
        set_rom();
    }

    void wrapper_sim_t::idle()
    { }

    void wrapper_sim_t::read_chunk(addr_t taddr, size_t len, void* dst)
    {
        assert(len == 8);
        auto data = debug_mmu->to_target(debug_mmu->load_uint64(taddr));
        memcpy(dst, &data, sizeof data);
    }

    void wrapper_sim_t::write_chunk(addr_t taddr, size_t len, const void* src)
    { 
        assert(len == 8);
        target_endian<uint64_t> data;
        memcpy(&data, src, sizeof data);
        debug_mmu->store_uint64(taddr, debug_mmu->from_target(data));
    }

    void wrapper_sim_t::set_target_endianness(memif_endianness_t endianness)
    {
        #ifdef RISCV_ENABLE_DUAL_ENDIAN
        assert(endianness == memif_endianness_little || endianness == memif_endianness_big);

        bool enable = endianness == memif_endianness_big;
        debug_mmu->set_target_big_endian(enable);
        for (size_t i = 0; i < procs.size(); i++) {
            procs[i]->get_mmu()->set_target_big_endian(enable);
            procs[i]->reset();
        }
        #else
        assert(endianness == memif_endianness_little);
        #endif
    }

    memif_endianness_t wrapper_sim_t::get_target_endianness() const
    {
        #ifdef RISCV_ENABLE_DUAL_ENDIAN
        return debug_mmu->is_target_big_endian()? memif_endianness_big : memif_endianness_little;
        #else
        return memif_endianness_little;
        #endif
    }

    void wrapper_sim_t::proc_reset(unsigned id)
    { }

    class spike_sim : public base_sim
    {
    private:
        uint64_t currentCycle;
        bool halted;
        bool dump_dts;
        size_t nprocs;
        const char *kernel;
        reg_t kernel_offset, kernel_size;
        size_t initrd_size;
        reg_t initrd_start, initrd_end;
        const char *bootargs;
        reg_t start_pc;
        std::vector<std::pair<reg_t, mem_t *>> mems;
        std::vector<std::pair<reg_t, abstract_device_t *>> plugin_devices;
        std::unique_ptr<icache_sim_t> ic;
        std::unique_ptr<dcache_sim_t> dc;
        std::unique_ptr<cache_sim_t> l2;
        const char *initrd;
        const char *isa;
        const char *priv;
        const char *varch;
        std::vector<int> hartids;
        int argc;
        char ** argv;
        std::vector<std::string> htif_args;

        // Simulation engine
        wrapper_sim_t *sim_eng;

        downstreamCPU_interface gem5ObjInterface;

        bool check_file_exists(const char *fileName);

        std::ifstream::pos_type get_file_size(const char *filename);

        void read_file_bytes(const char *filename, size_t fileoff,
                             mem_t *mem, size_t memoff, size_t read_sz);

        static bool sort_mem_region(const std::pair<reg_t, mem_t *> &a,
                             const std::pair<reg_t, mem_t *> &b);

        void merge_overlapping_memory_regions(
                        std::vector<std::pair<reg_t, mem_t *>> &mems);

        void cleanup();

        class gem5_memory_t : public mem_t {
            private:
                std::string strParams;
                spike_sim * parent_sim;
                uint64_t size;
                uint64_t base_addr;

            public:
                gem5_memory_t(std::string params) : strParams(params) {
                    auto pos = params.find(" ");
                    auto pos2 = params.find(" ", pos+1);
                    if (pos != std::string::npos && pos2 != std::string::npos) {
                        std::string sizeStr = params.substr(0, pos);
                        std::string basePtrStr = params.substr(pos + 1, pos2);
                        std::string ptrStr = params.substr(pos2 + 1, params.length());
                        size = std::stoul(sizeStr);
                        base_addr = std::stoul(basePtrStr);
                        parent_sim = reinterpret_cast<spike_sim *> (std::stoul(ptrStr));
                    } else {
                        printf("Error processing arguments for gem5_memory_t (size, ptrMem, ptrMutex)");
                        size = 0;
                        base_addr = 0;
                    }
                }

                bool load(reg_t addr, size_t len, uint8_t* bytes, bool atomic) {
                    auto dsif = parent_sim->getDownstreamInterface();
                    dsif.load(base_addr+addr, len, bytes, atomic);
                    parent_sim->get_sim_eng()->wait_for_mem();
                    return true;
                }

                bool store(reg_t addr, size_t len, const uint8_t* bytes, bool atomic) {
                    auto dsif = parent_sim->getDownstreamInterface();
                    dsif.store(base_addr+addr, len, bytes, atomic);
                    parent_sim->get_sim_eng()->wait_for_mem();
                    return true;
                }
        };

        static mmio_plugin_registration_t<gem5_memory_t> register_gem5Mem;

    public:
        spike_sim(downstreamCPU_interface obj, int argc, char ** argv);

        void parse_args();

        void init();
        int run();

        void registerSpMem(reg_t base_addr, uint64_t size);
        void registerControlSignals(reg_t base_addr, uint64_t size);
        void registerMainMem(reg_t base_addr, uint64_t size);
        bool load_kernel();
        bool load_initrd_file();

        wrapper_sim_t * get_sim_eng() {
            return this->sim_eng;
        }

        std::vector<std::pair<reg_t, mem_t *>> make_mems(const char *arg);
        
        static void show_error() {
            printf("Error with spike arguments\n");
        }

        unsigned long atoul_safe(const char* s)
        {
        char* e;
        auto res = strtoul(s, &e, 10);
        if (*e)
            show_error();
        return res;
        }

        unsigned long atoul_nonzero_safe(const char* s)
        {
        auto res = atoul_safe(s);
        if (!res)
            show_error();
        return res;
        }

        ~spike_sim()
        {
            cleanup();
        }

        void steps(uint64_t n, bool blocked) {
            // printf("Cycle = %lu to %lu \n", currentCycle, currentCycle + n);
            // debugger_connector->performSimSteps(n);

            sim_eng->set_steps_n(n);
            sim_eng->set_blocked(blocked);
            sim_eng->continue_spike_sim();

            currentCycle += n;
        }


        uint64_t getPC() {
            return sim_eng->getPC();
        }
        std::string disasm_cur_inst() {
            return sim_eng->disasm_cur_inst();
        }

        // Getters and setters
        bool get_halted() { return halted; }
        void set_halted(bool a_halted) { halted = a_halted; }
        bool get_dump_dts() { return dump_dts; }
        void set_dump_dts(bool a_dump_dts) { dump_dts = a_dump_dts; }
        size_t get_nprocs() { return nprocs; }
        void set_nprocs(size_t a_nprocs) { nprocs = a_nprocs; }
        const char *get_kernel() { return kernel; }
        void set_kernel(const char *a_kernel) { kernel = a_kernel; }
        reg_t get_kernel_offset() { return kernel_offset; }
        void set_kernel_offset(reg_t a_kernel_offset) {
            kernel_offset = a_kernel_offset;
        }
        reg_t get_kernel_size() { return kernel_size; }
        void set_kernel_size(reg_t a_kernel_size) {
            kernel_size = a_kernel_size;
        }
        size_t get_initrd_size() { return initrd_size; }
        void set_initrd_size(size_t a_initrd_size) {
            initrd_size = a_initrd_size;
        }
        reg_t get_initrd_start() { return initrd_start; }
        void set_initrd_start(reg_t a_initrd_start) {
            initrd_start = a_initrd_start;
        }
        reg_t get_initrd_end() { return initrd_end; }
        void set_initrd_end(reg_t a_initrd_end) { initrd_end = a_initrd_end; }
        const char *get_bootargs() { return bootargs; }
        void set_bootargs(const char *a_bootargs) { bootargs = a_bootargs; }
        reg_t get_start_pc() { return start_pc; }
        void set_start_pc(reg_t a_start_pc) { start_pc = a_start_pc; }
        std::vector<std::pair<reg_t, mem_t *>> get_mems() { return mems; }
        void set_mems(std::vector<std::pair<reg_t, mem_t *>> a_mems) {
            mems = a_mems;
        }
        std::vector<std::pair<reg_t, abstract_device_t *>>
            get_plugin_devices() {
                return plugin_devices;
            }
        void set_plugin_devices(
            std::vector<std::pair<reg_t, abstract_device_t *>>
            a_plugin_devices) {
                plugin_devices = a_plugin_devices;
            }
        std::unique_ptr<icache_sim_t>& get_ic() { return ic; }
        std::unique_ptr<dcache_sim_t>& get_dc() { return dc; }
        std::unique_ptr<cache_sim_t>& get_l2() { return l2; }

        const char *get_isa() { return isa; }
        void set_isa(const char *a_isa) { isa = a_isa; }
        const char *get_priv() { return priv; }
        void set_priv(const char *a_priv) { priv = a_priv; }
        const char *get_varch() { return varch; }
        void set_varch(const char *a_varch) { varch = a_varch; }
        std::vector<int> get_hartids() { return hartids; }
        void set_hartids(std::vector<int> a_hartids) { hartids = a_hartids; }

        downstreamCPU_interface getDownstreamInterface() { return gem5ObjInterface; }
    };

        spike_sim::spike_sim(downstreamCPU_interface obj, int argc, char ** argv) : 
                  currentCycle(0),
                  halted(false),
                  dump_dts(false),
                  nprocs(1),
                  kernel(nullptr),
                  initrd_start(0),
                  initrd_end(0),
                  bootargs(nullptr),
                  start_pc(reg_t(-1)),
                  initrd(nullptr),
                  isa(DEFAULT_ISA),
                  priv(DEFAULT_PRIV),
                  varch(DEFAULT_VARCH),
                  argc(argc), argv(argv),
                  gem5ObjInterface(obj) {}
    
    void
    spike_sim::parse_args() {
        option_parser_t parser;
        
        auto const hartids_parser = [&](const char *s)
        {
            std::string const str(s);
            std::stringstream stream(str);

            int n;
            while (stream >> n)
            {
                hartids.push_back(n);
                if (stream.peek() == ',')
                    stream.ignore();
            }
        };

        auto const device_parser = [&](const char *s) {
            const std::string str(s);
            std::istringstream stream(str);

            // We are parsing a string like name,base,args.

            // Parse the name, which is simply all of
            // the characters leading up to the
            // first comma. The validity of the plugin
            // name will be checked later.
            std::string name;
            std::getline(stream, name, ',');
            if (name.empty())
            {
                throw std::runtime_error("Plugin name is empty.");
            }

            // Parse the base address. First, get all of
            // the characters up to the next comma (or up
            // to the end of the string if there is no comma).
            // Then try to parse that string as an
            // integer according to the rules of strtoull. It
            // could be in decimal, hex, or octal.
            // Fail if we were able to parse a
            // number but there were garbage characters
            //after the valid number. We must
            // consume the entire string between the commas.
            std::string base_str;
            std::getline(stream, base_str, ',');
            if (base_str.empty())
            {
                throw std::runtime_error("Device base address is empty.");
            }
            char *end;
            reg_t base = static_cast<reg_t>(strtoull(base_str.c_str(), &end, 0));
            if (end != &*base_str.cend())
            {
                throw std::runtime_error("Error parsing device base address.");
            }

            // The remainder of the string is the arguments.
            // We could use getline, but that could ignore
            // newline characters in the arguments. That should be
            // rare and discouraged, but handle it
            // here anyway with this weird in_avail technique.
            // The arguments are optional, so if there
            // were no arguments specified we could end
            // up with an empty string here. That's okay.
            auto avail = stream.rdbuf()->in_avail();
            std::string args(avail, '\0');
            stream.readsome(&args[0], avail);

            plugin_devices.emplace_back(base,
                                    new mmio_plugin_device_t(name, args));
        };


        parser.help(show_error);

        parser.option('h', "help", 0, [&](const char* s){show_error();});
        parser.option('p', 0, 1, [&](const char* s){nprocs = atoul_nonzero_safe(s);});
        parser.option('m', 0, 1, [&](const char* s){mems = make_mems(s);});
        // I wanted to use --halted, but for some reason that doesn't work.
        parser.option('H', 0, 0, [&](const char* s){halted = true;});
        parser.option(0, "pc", 1, [&](const char* s){start_pc = strtoull(s, 0, 0);});
        parser.option(0, "hartids", 1, hartids_parser);
        parser.option(0, "ic", 1, [&](const char* s){ic.reset(new icache_sim_t(s));});
        parser.option(0, "dc", 1, [&](const char* s){dc.reset(new dcache_sim_t(s));});
        parser.option(0, "l2", 1, [&](const char* s){l2.reset(cache_sim_t::construct(s, "L2$"));});
        parser.option(0, "isa", 1, [&](const char* s){isa = s;});
        parser.option(0, "priv", 1, [&](const char* s){priv = s;});
        parser.option(0, "varch", 1, [&](const char* s){varch = s;});
        parser.option(0, "device", 1, device_parser);
        parser.option(0, "dump-dts", 0, [&](const char *s){dump_dts = true;});
        parser.option(0, "kernel", 1, [&](const char* s){kernel = s;});
        parser.option(0, "initrd", 1, [&](const char* s){initrd = s;});
        parser.option(0, "bootargs", 1, [&](const char* s){bootargs = s;});
        parser.option(0, "extlib", 1, [&](const char *s){
            void *lib = dlopen(s, RTLD_NOW | RTLD_GLOBAL);
            if (lib == NULL) {
            fprintf(stderr, "Unable to load extlib '%s': %s\n", s, dlerror());
            exit(-1);
            }
        });

        auto argv1 = parser.parse(argv);
        htif_args = std::vector<std::string>(argv1, (const char*const*)argv + argc);
        if (mems.empty())
            mems = make_mems("2048");

        if (!*argv1)
            show_error();

        // Set Hart ID
        hartids.push_back(gem5ObjInterface.hartid);
    }

    bool
    spike_sim::check_file_exists(const char *fileName)
    {
        std::ifstream infile(fileName);
        return infile.good();
    }

    std::ifstream::pos_type
    spike_sim::get_file_size(const char *filename)
    {
        std::ifstream in(filename, std::ios::ate | std::ios::binary);
        return in.tellg();
    }

    void
    spike_sim::read_file_bytes(const char *filename, size_t fileoff,
                         mem_t *mem, size_t memoff, size_t read_sz)
    {
        std::ifstream in(filename, std::ios::in | std::ios::binary);
        in.seekg(fileoff, std::ios::beg);

        std::vector<char> read_buf(read_sz, 0);
        in.read(&read_buf[0], read_sz);
        mem->store(memoff, read_sz, (uint8_t *)&read_buf[0], false);
    }

    bool
    spike_sim::sort_mem_region(const std::pair<reg_t, mem_t *> &a,
                         const std::pair<reg_t, mem_t *> &b)
    {
        if (a.first == b.first)
            return (a.second->size() < b.second->size());
        else
            return (a.first < b.first);
    }

    void
    spike_sim::merge_overlapping_memory_regions(
        std::vector<std::pair<reg_t, mem_t *>> &mems)
    {
        // check the user specified memory regions and merge the overlapping or
        // eliminate the containing parts
        std::sort(mems.begin(), mems.end(), sort_mem_region);
        std::vector<std::pair<reg_t, mem_t *>>::iterator it = mems.begin() + 1;

        while (it != mems.end())
        {
            reg_t start = prev(it)->first;
            reg_t end = prev(it)->first + prev(it)->second->size();
            reg_t start2 = it->first;
            reg_t end2 = it->first + it->second->size();

            //contains -> remove
            if (start2 >= start && end2 <= end)
            {
                it = mems.erase(it);
                //parital overlapped -> extend
            }
            else if (start2 >= start && start2 < end)
            {
                delete prev(it)->second;
                prev(it)->second = new mem_t(std::max(end, end2) - start);
                it = mems.erase(it);
                // no overlapping -> keep it
            }
            else
            {
                it++;
            }
        }
    }

    void
    spike_sim::cleanup()
    {
        for (auto &mem : mems)
            delete mem.second;

        for (auto &plugin_device : plugin_devices)
            delete plugin_device.second;

        delete sim_eng;
    }



    void
    spike_sim::init()
    {
        parse_args();
        load_kernel();
        load_initrd_file();
        // Always start halted. Gem5 will manage clock progress
        // set_halted(true);
        sim_eng = new wrapper_sim_t(isa, priv, varch, nprocs, halted,
                                    initrd_start, initrd_end,
                                    bootargs, start_pc,
                                    mems, plugin_devices, htif_args,
                                    std::move(hartids), this);

        if (dump_dts)
        {
            printf("%s", sim_eng->get_dts());
            return;
        }

        if (ic && l2)
            ic->set_miss_handler(&*l2);
        if (dc && l2)
            dc->set_miss_handler(&*l2);

        for (size_t i = 0; i < nprocs; i++)
        {
            if (ic)
                sim_eng->get_core(i)->get_mmu()->register_memtracer(&*ic);
            if (dc)
                sim_eng->get_core(i)->get_mmu()->register_memtracer(&*dc);
        }
    }

    int
    spike_sim::run()
    {
        int res = 0;
        try {
            // debugger_connector->activateDMI();
            res = sim_eng->run();
        } catch(const std::runtime_error& ex) {
            std::clog << ex.what() << std::endl;
        } catch (...) {
            printf("There was an error running spike\n");
            std::exception_ptr p = std::current_exception();
            std::clog <<(p ? p.__cxa_exception_type()->name() : "null") << std::endl;
        }

        return res;
    }

    // Registering plugin name 
    mmio_plugin_registration_t<spike_sim::gem5_memory_t> spike_sim::register_gem5Mem("gem5_memory_t");


    void
    spike_sim::registerSpMem(reg_t base_addr, uint64_t size) {
        std::string params;

        params += std::to_string(size) + " ";
        params += std::to_string(base_addr) + " ";
        params += std::to_string(reinterpret_cast<uint64_t>(this));

        plugin_devices.emplace_back(base_addr,
                                    new mmio_plugin_device_t("gem5_memory_t", params));
    }

    void
    spike_sim::registerControlSignals(reg_t base_addr, uint64_t size) {
        std::string params;

        params += std::to_string(size) + " ";
        params += std::to_string(base_addr) + " ";
        params += std::to_string(reinterpret_cast<uint64_t>(this));

        plugin_devices.emplace_back(base_addr,
                                    new mmio_plugin_device_t("gem5_memory_t", params));
    }

    void
    spike_sim::registerMainMem(reg_t base_addr, uint64_t size) {
        std::string params;

        params += std::to_string(size) + " ";
        params += std::to_string(base_addr) + " ";
        params += std::to_string(reinterpret_cast<uint64_t>(this));

        plugin_devices.emplace_back(base_addr,
                                    new mmio_plugin_device_t("gem5_memory_t", params));
    }

    bool
    spike_sim::load_kernel()
    {
        if (kernel && check_file_exists(kernel))
        {
            kernel_size = get_file_size(kernel);
            if (isa[2] == '6' && isa[3] == '4')
                kernel_offset = 0x200000;
            else
                kernel_offset = 0x400000;
            for (auto &m : mems)
            {
                if (kernel_size && (kernel_offset +
                                    kernel_size) < m.second->size())
                {
                    read_file_bytes(kernel, 0, m.second,
                                    kernel_offset, kernel_size);
                    return true;
                }
            }
        }
        return false;
    }

    bool
    spike_sim::load_initrd_file()
    {
        if (initrd && check_file_exists(initrd))
        {
            initrd_size = get_file_size(initrd);
            for (auto &m : mems)
            {
                if (initrd_size && (initrd_size + 0x1000) < m.second->size())
                {
                    initrd_end = m.first + m.second->size() - 0x1000;
                    initrd_start = initrd_end - initrd_size;
                    read_file_bytes(initrd, 0, m.second,
                                    initrd_start - m.first, initrd_size);
                    return true;
                }
            }
        }
        return false;
    }

    std::vector<std::pair<reg_t, mem_t *>>
    spike_sim::make_mems(const char *arg)
    {
        // handle legacy mem argument
        char *p;
        auto mb = strtoull(arg, &p, 0);
        if (*p == 0)
        {
            reg_t size = reg_t(mb) << 20;
            if (size != (size_t)size)
                throw std::runtime_error("Size would overflow size_t");
            return std::vector<std::pair<reg_t, mem_t *>>(1,
                    std::make_pair(reg_t(DRAM_BASE), new mem_t(size)));
        }

        // handle base/size tuples
        std::vector<std::pair<reg_t, mem_t *>> res;
        while (true)
        {
            auto base = strtoull(arg, &p, 0);
            if (!*p || *p != ':')
                throw std::runtime_error("Invalid spike memory "
                                         "format specification");
            auto size = strtoull(p + 1, &p, 0);

            // page-align base and size
            auto base0 = base, size0 = size;
            size += base0 % PGSIZE;
            base -= base0 % PGSIZE;
            if (size % PGSIZE != 0)
                size += PGSIZE - size % PGSIZE;

            if (base + size < base)
                throw std::runtime_error("Invalid spike memory"
                                         " format specification");

            if (size != size0)
            {
                fprintf(stderr, "Warning: the memory at  [0x%llX, 0x%llX]"
                                " has been realigned\n"
                                "to the %ld KiB page size: [0x%llX, 0x%llX]\n",
                        base0, base0 + size0 - 1, long(PGSIZE / 1024),
                        base, base + size - 1);
            }

            res.push_back(std::make_pair(reg_t(base), new mem_t(size)));
            if (!*p)
                break;
            if (*p != ',')
                throw std::runtime_error("Invalid spike memory"
                                         "format specification");
            arg = p + 1;
        }

        merge_overlapping_memory_regions(res);
        return res;
    }

    sim::sim(downstreamCPU_interface obj, int argc, char ** argv) {
        sim_ptr = std::make_unique<spike_sim>(obj, argc, argv);
    }

    void
    sim::init() {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        ptr->init();
    }

    void
    sim::registerSpMem(uint64_t addr, uint64_t size) {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        ptr->registerSpMem(addr,size);
    }

    void
    sim::registerControlSignals(uint64_t addr, uint64_t size) {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        ptr->registerControlSignals(addr,size);
    }

    uint64_t
    sim::getPC() {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        return ptr->getPC();
    }

    std::string
    sim::disasm_cur_inst() {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        return ptr->disasm_cur_inst();
    }

    void
    sim::registerMainMem(uint64_t addr, uint64_t size) {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        ptr->registerMainMem(addr,size);
    }

    void
    sim::run() {
        spike_sim *ptr = dynamic_cast<spike_sim*>(this->get_sim_ptr().get());
        ptr->run();
        // pthread_create(&sim_thread, nullptr, [](void * self) -> void*{
        //     sim* sim_obj = static_cast<sim*>(self);
        //     spike_sim *ptr = dynamic_cast<spike_sim*>(sim_obj->get_sim_ptr().get());
        //     ptr->run();
        //     return (void *)nullptr;
        // }, this);
    }

    void
    sim::steps(uint64_t n, bool blocked) {
        spike_sim *ptr = dynamic_cast<spike_sim*>(sim_ptr.get());
        ptr->steps(n, blocked);
    }

    sim::~sim() {
    }

    wrapper_sim_t::wrapper_sim_t(const char* isa, const char* priv,
        const char* varch, size_t nprocs,
        bool halted, reg_t initrd_start, reg_t initrd_end,
        const char* bootargs, reg_t start_pc,
        std::vector<std::pair<reg_t, mem_t*>> mems,
        std::vector<std::pair<reg_t, abstract_device_t*>> plugin_devices,
        const std::vector<std::string>& args, 
        const std::vector<int> hartids, spike_sim* parent) : 
            htif_t(args),
            current_step(0),
            parent_sim(parent),
            steps_n(0),
            blocked(false),
            mems(mems),
            plugin_devices(plugin_devices),
            procs(std::max(nprocs, size_t(1))),
            initrd_start(initrd_start),
            initrd_end(initrd_end),
            bootargs(bootargs),
            start_pc(start_pc),
            sout_(nullptr) {
                sout_.rdbuf(std::cerr.rdbuf()); // debug output goes to stderr by default

                for (auto& x : mems)
                    bus.add_device(x.first, x.second);

                for (auto& x : plugin_devices)
                    bus.add_device(x.first, x.second);

                debug_mmu = new mmu_t(this, NULL);

                if (! (hartids.empty() || hartids.size() == nprocs)) {
                    std::cerr << "Number of specified hartids ("
                                << hartids.size()
                                << ") doesn't match number of processors ("
                                << nprocs << ").\n";
                    exit(1);
                }

                for (size_t i = 0; i < nprocs; i++) {
                    int hart_id = hartids.empty() ? i : hartids[i];
                    procs[i] = new processor_t(isa, priv, varch, this, hart_id, halted,
                                            stderr, sout_);
                }

                make_dtb();
                assert(nprocs > 0);
                for (auto & current_proc : procs) {
                    current_proc->halt_request = current_proc->HR_NONE;
                }
            }

    void
    wrapper_sim_t::step() {
        gem5_ctx->switch_to();
        while(!signal_exit && exitcode == 0) {
            // When blocked, there should not be progress in the
            // processor, just htif
            if (!blocked) {
                current_step+=steps_n;
                if (!done()) {
                    for (size_t i = 0, steps = 0; i < steps_n; i += steps) {
                        steps = steps_n - i;
                        for (auto & current_proc : procs) {
                            current_proc->step(steps);
                            current_proc->get_mmu()->yield_load_reservation();
                        }
                    }
                } else {
                    auto dsif = parent_sim->getDownstreamInterface();
                    dsif.end_of_program(exit_code());
                }
            }
            // htif goes 100 tiemes "faster"
            for (int i = 0; i < 100; i++) {
                if (!signal_exit && exitcode == 0) {
                    htif_step();
                } else {
                    parent_sim->getDownstreamInterface().end_of_program(exit_code());
                    break;
                }
            }
            gem5_ctx->switch_to();
        }
    }
} // namespace
