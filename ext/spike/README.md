Follow these steps to get spike as part of gem5

1. Download spike simulator (Default SPIKE_DIR)
    1.1 Go to ext/spike (this directory)
    1.2 Clone DRAMSim2: git clone https://github.com/riscv/riscv-isa-sim.git

2. Compile gem5
    2.1 Business as usual

You can use the `SPIKE_DIR` option or the `GEM5_SPIKE_DIR` env variable to change the location of the repo