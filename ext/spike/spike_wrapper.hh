#ifndef __SPIKE_OBJECT_CPU_HH__
#define __SPIKE_OBJECT_CPU_HH__
/**
 * @file spike_wrapper.hh
 * @author Jose M Monsalve Diaz (jmonsalvediaz@anl.gov)
 * @brief Glue logic to connect to the spike sim without a terminal
 * @version 0.1
 * @date 2021-06-28
 *
 * @copyright Copyright (c) 2021
 *
 * The spike simulator is available through a terminal CLI app.
 * If we want to connect it to Gem5 we need to unwrap this app
 * to allow access to the underlying logic.
 *
 *
 */

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <memory>
#include <string>
#include <vector>
#include <functional>
#include <pthread.h>

namespace spike
{

    struct downstreamCPU_interface {
        uint64_t hartid; // Lane ID
        std::function<void(uint64_t ,uint64_t, void *, bool)> load;
        std::function<void(uint64_t ,uint64_t, void *, bool)> load_functional;
        std::function<void(uint64_t ,uint64_t, const void *, bool)> store;
        std::function<void(uint64_t ,uint64_t, const void *, bool)> store_functional;
        std::function<void(int64_t)> end_of_program;
    };

    class base_sim
    {
    public:
        base_sim() {}
        virtual void steps(uint64_t n, bool blocked) = 0;
        virtual ~base_sim() {}
    };

    class sim
    {
    private:
        std::unique_ptr<base_sim> sim_ptr;

    public:
        sim(downstreamCPU_interface obj, int argc, char ** argv);
        void init();
        void run();
        void registerSpMem(uint64_t baddr, uint64_t size);
        void registerControlSignals(uint64_t baddr, uint64_t size);
        void registerMainMem(uint64_t baddr, uint64_t size);
        void steps(uint64_t n, bool blocked);
        uint64_t getPC();
        std::string disasm_cur_inst();

        std::unique_ptr<base_sim> & get_sim_ptr() { return sim_ptr; } 

        // TODO: Send a wrapper function to communicate with Gem5 when there are reads or writes to DRAM. 
        void registerDRAMread();
        void registerDRAMwrite();
        ~sim();
    };

} // namespace spike

#endif //__DOWNSTREAM_OBJECT_CPU_HH__