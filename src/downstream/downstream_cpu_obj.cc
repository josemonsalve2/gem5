/**
 * @file downstream_obj_cpu.cc
 * @author Jose M Monsalve Diaz (jmonsalvediaz@anl.gov)
 * @brief Definition of a simple downstream object
 * @version 0.1
 * @date 2021-06-28
 *
 * @copyright Copyright (c) 2021
 *
 * This file is based on the simple_cache.cc
 * example in the learning_gem5 folder.
 * The following Copyright applies:
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */

#include "downstream/downstream_cpu_obj.hh"

#include <iostream>

#include "base/random.hh"
#include "cpu/simple_thread.hh"
#include "debug/Downstream.hh"
#include "debug/DownstreamCPU.hh"
#include "debug/DownstreamCPUTicks.hh"
#include "sim/syscall_emul.hh"
#include "sim/system.hh"

#define SPIKE_COMMAND "spike"

DownstreamCPUObj::DownstreamCPUObj(
                    const gem5::DownstreamCPUObjParams &params) :
    BaseCPU(params),
    stats(this),
    stall_begin(0),
    memPort(params.name + ".sysmem_side", this,
            &stats.rdDRAMAccesses, &stats.wrDRAMAccesses,
            DownstreamCPUObj::MemSidePort::MemType::DRAM),
    spmemPort(params.name + ".spmem_side", this,
            &stats.rdSPAccesses, &stats.wrSPAccesses,
            DownstreamCPUObj::MemSidePort::MemType::SPMEM),
    lane_id(params.lane_id),
    ctx_id(params.context_id),
    spikeTick(params.sync_clock),
    tickEvent([this]{ tick(); }, "Downstream tick",
        false, gem5::Event::CPU_Tick_Pri),
    retryEvent([this]{ retryMemory(); }, "Downstream retry event",
        false, gem5::Event::CPU_Tick_Pri)
{

    for (unsigned i = 0; i < numThreads; i++) {
        threadContexts.push_back(
            (new gem5::SimpleThread(this, 0, params.system,
                         params.workload[i], params.mmu,
                         params.isa[i]))->getTC());
    }

    DPRINTF(DownstreamCPU, "Creating downstream Lane [%d] "\
                            "with contextId = %d\n", lane_id, ctx_id);

    std::string spike_args = params.spikeArgs;

    // If command does not start with "spike ", we add it
    if (spike_args.substr(0,
            std::string(SPIKE_COMMAND).length()) != SPIKE_COMMAND)
        spike_args = std::string(SPIKE_COMMAND" ") + spike_args;

    // Clean double spaces and spaces at beginning and end
    std::size_t double_spaces =
        spike_args.find("  ");
    while (double_spaces != std::string::npos) {
        spike_args.erase(double_spaces,1);
        double_spaces = spike_args.find("  ");
    }
    while (spike_args.back() == ' ') spike_args.pop_back();
    while (spike_args.front() == ' ') spike_args.erase(0,1);


    // Count args
    std::size_t num_args = 1;
    for (auto it = spike_args.begin(); it != spike_args.end(); ++it)
        if (*it == ' ')
            num_args++;

    // Make a copy to send to spike
    // We allocate only once, and then create pointers
    // relative to the first location
    // We change spaces to \0.
    // For example, if spike_args = "spike pk ./a.out"
    // spike_sim_args[0] will be allocated and
    // "spike pk ./a.out" copied over
    // spike_sim_args[1] = &spike_sim_args[0][6]
    // (e.g. character p in "pk")
    // spike_sim_args[2] = &spike_sim_args[0][9]
    // (e.g. character . in "./a.out")
    // spaces are changed to \0,
    // resulting in spike_sim_args[0] = "spike\0pk\0./a.out"
    // creating independent char[] per argument
    spike_sim_args = new char*[num_args];
    spike_sim_args[0] = new char[spike_args.length()+1];
    std::strcpy(spike_sim_args[0], spike_args.c_str());

    DPRINTF(DownstreamCPU,
            "Initializing spike with arguments %s\n", spike_sim_args[0]);

    // Assign all the other pointers
    std::size_t curArg = 1;
    for (std::size_t curChar = 0; curChar < spike_args.length(); curChar++) {
        if (spike_args[curChar] == ' ') {
            spike_sim_args[curArg++] = spike_sim_args[0] + curChar + 1;
            // Change space by end of char e.g. "spike pk" to "spike\0pk"
            spike_sim_args[0][curChar] = '\0';
        }
    }

    // We create an interface to avoid having to send the whole
    // DownstreamCPUObj, which will require extra compilation
    // dependencies that, due to the natureof the two projects
    // (Spike and Gem5), they become difficult to manage.
    spike::downstreamCPU_interface interface = {
        lane_id,
        [&] (uint64_t addr, uint64_t size, void * result, bool atomic) {
            sendReadRequestMem(addr, size, result, atomic);
        },
        [&] (uint64_t addr, uint64_t size, void * result, bool atomic) {
            sendReadRequestFuncMem(addr, size, result, atomic);
        },
        [&] (uint64_t addr, uint64_t size, const void * result, bool atomic) {
            sendWriteRequestMem(addr, size, result, atomic);
        },
        [&] (uint64_t addr, uint64_t size, const void * result, bool atomic) {
            sendWriteRequestFuncMem(addr, size, result, atomic);
        },
        [&] (int64_t ret_code) {
            endOfProgram(ret_code);
        },
    };

    // register Control Ops
    gem5::Addr ctrlAddr = params.downstream_range.start()
                          + params.downstream_range.size();

    // Registering get cycles
    ctrlOp[ctrlAddr] = [&](void * val) {
        uint64_t* cycle = static_cast<uint64_t*>(val);
        *cycle = curCycle();
    };
    // Registering reset Stats
    ctrlOp[ctrlAddr+8] = [&](void * val) {
        stats.resetStats();
    };
    DPRINTF(DownstreamCPU,
            "Initializing ctrlOp Get Cycles in address %p\n", ctrlAddr);

    spike_sim = new spike::sim(interface, num_args, spike_sim_args);

    dramMemAddrRanges.push_back(params.shared_dram_range);
    downstreamAddrRanges.push_back(params.downstream_range);

    scheduleTick(gem5::Cycles(0));

    memPort.getAddrRanges();
    spmemPort.getAddrRanges();

}

DownstreamCPUObj::MemSidePort *
DownstreamCPUObj::findPort(gem5::Addr addr)
{
    for ( auto & range : memPort.getAddrRanges()) {
        if (addr >= range.start() && addr < range.end()) {
            return &memPort;
        }
    }
    for ( auto & range : spmemPort.getAddrRanges()) {
        if (addr >= range.start() && addr < range.end()) {
            return &spmemPort;
        }
    }
    panic("Port not found for addr %p, will cause a segfault", addr);
    return nullptr;
}

bool DownstreamCPUObj::checkControl(gem5::Addr addr) {
    return ctrlOp.find(addr) != ctrlOp.end();
}


gem5::PacketPtr
DownstreamCPUObj::createReadPacket(gem5::Addr addr,
                    uint64_t size, void * result, bool atomic)
{
    gem5::RequestPtr request = nullptr;
    if (atomic) {
        atomicOp = std::make_unique<SpikeAtomicOpFunctor>(
                static_cast<uint8_t*> (result),
                [&] (uint8_t * res_addr, uint8_t * addr)
            {
                uint64_t * temp = reinterpret_cast<uint64_t*> (res_addr);
                *temp = reinterpret_cast<uint64_t>(addr);
                // Passing the pointer by value in the result memory
                DPRINTF(DownstreamCPU,
                    "Continue atomic operation in spike. "\
                    "With Addr %p\n", reinterpret_cast<uint64_t>(addr));
                spike_sim->steps(1, false);
            }
        );
        request =
            std::make_shared<gem5::Request>(addr,
                    size,
                    gem5::Request::Flags(gem5::Request::ATOMIC_RETURN_OP),
                    getRequestorID(),
                    0, // PC
                    ctx_id,
                    std::move(atomicOp));
        request->setPaddr(addr);
    } else {
        request = std::make_shared<gem5::Request>(addr,
                    size, gem5::Request::Flags(), getRequestorID());
    }

    gem5::PacketPtr packet = new gem5::Packet(request,
                                atomic ? gem5::MemCmd::SwapReq :
                                gem5::Packet::makeReadCmd(request));

    packet->dataStatic(result);

    return packet;
}

gem5::PacketPtr
DownstreamCPUObj::createWritePacket(gem5::Addr addr,
                    uint64_t size, const void * result, bool atomic)
{
    gem5::RequestPtr request = nullptr;
    request = std::make_shared<gem5::Request>(addr, size,
        gem5::Request::Flags(), getRequestorID());
    request->setContext(ctx_id);
    gem5::PacketPtr packet =
        new gem5::Packet(request, gem5::MemCmd::WriteReq);
    packet->dataStaticConst(static_cast<const unsigned char *>(result));
    return packet;
}

void
DownstreamCPUObj::
sendReadRequestMem(gem5::Addr addr,
                    uint64_t size, void * result, bool atomic)
{
    if (checkControl(addr)) {
        DPRINTF(DownstreamCPU, "Control Operation\n");
        stats.ctrlAccess++;
        ctrlOp[addr](result);
        return;
    };

    DPRINTF(DownstreamCPU,
        "Sending Read Request to Memory: Atomic? %d, Addr = %X, size = %d\n",
        atomic, addr, size);
    DownstreamCPUObj::MemSidePort * port = findPort(addr);
    gem5::PacketPtr packet = createReadPacket(addr, size, result, atomic);
    port->sendPacket(packet);

    // Begin stalling
    descheduleTick();
    stall_begin = curCycle();
    cur_type = port->getType();
}

void
DownstreamCPUObj::sendReadRequestFuncMem(gem5::Addr addr,
                    uint64_t size, void * result, bool atomic)
{
    if (checkControl(addr)) {
        DPRINTF(DownstreamCPU, "Control Operation\n");
        stats.ctrlAccess++;
        ctrlOp[addr](result);
        return;
    };
    DPRINTF(DownstreamCPU,
        "Sending Functional Read Request to Memory: Addr = %X, size = %d\n",
        addr, size);
    DownstreamCPUObj::MemSidePort * port = findPort(addr);
    gem5::PacketPtr packet = createReadPacket(addr, size, result, atomic);

    port->sendPacketFunctional(packet);
}

void
DownstreamCPUObj::sendWriteRequestMem(gem5::Addr addr,
                    uint64_t size, const void * result,
                    bool atomic)
{
    if (checkControl(addr)) {
        DPRINTF(DownstreamCPU, "Control Operation\n");
        stats.ctrlAccess++;
        ctrlOp[addr](const_cast<void*>(result));
        return;
    };
    DPRINTF(DownstreamCPU,
        "Sending Write Request to Memory: Atomic? %d, Addr = %X, size = %d\n",
        atomic, addr, size);
    DownstreamCPUObj::MemSidePort * port = findPort(addr);
    auto packet = createWritePacket(addr,size,result,atomic);
    port->sendPacket(packet);
    // Begin stalling
    descheduleTick();
    stall_begin = curCycle();
    cur_type = port->getType();
}

void
DownstreamCPUObj::sendWriteRequestFuncMem(gem5::Addr addr,
                    uint64_t size, const void * result,
                    bool atomic)
{
    if (checkControl(addr)) {
        DPRINTF(DownstreamCPU, "Control Operation\n");
        stats.ctrlAccess++;
        ctrlOp[addr](const_cast<void*>(result));
        return;
    };
    DPRINTF(DownstreamCPU,
        "Sending Functional Write Request to Memory: Addr = %X, size = %d\n",
        addr, size);
    DownstreamCPUObj::MemSidePort * port = findPort(addr);
    auto packet = createWritePacket(addr,size,result,atomic);
    port->sendPacketFunctional(packet);
}

gem5::Port &
DownstreamCPUObj::getPort(const std::string &if_name, gem5::PortID idx)
{
    // This is the name from the Python DownstreamCPUObj
    // declaration in DownstreamCPUObj.py
    if (if_name == "mem_side") {
        // We should have already created all of the ports in the constructor
        return memPort;
    } if (if_name == "spmem_side") {
        // We should have already created all of the ports in the constructor
        return spmemPort;
    } else {
        // pass it along to our super class
        return ClockedObject::getPort(if_name, idx);
    }
}

void
DownstreamCPUObj::MemSidePort::sendPacketFunctional(gem5::PacketPtr pkt)
{
    // Note: This flow control is very simple since the cache is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");

    // If we can't send the packet across the port, store it for later.
    sendFunctional(pkt);
}

void
DownstreamCPUObj::MemSidePort::sendPacket(gem5::PacketPtr pkt)
{
    // Note: This flow control is very simple since the cache is blocking.
    DPRINTF(DownstreamCPU, "Sending packet %s\n", pkt->print());

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");

    // If we can't send the packet across the port, store it for later.
    if (!sendTimingReq(pkt)) {
        DPRINTF(DownstreamCPU, "DownstreamCPU: Blocking packet\n");
        blockedPacket = pkt;
        block_retry = true;
    } else {
        if (pkt->isRead())
            (*rdAccessStats)++;
        if (pkt->isWrite())
            (*wrAccessStats)++;
    }
}


bool
DownstreamCPUObj::MemSidePort::recvTimingResp(gem5::PacketPtr pkt)
{
    // Just forward to the cache.
    return owner->handleResponse(pkt);
}

void
DownstreamCPUObj::MemSidePort::recvReqRetry()
{
    DPRINTF(DownstreamCPU, "DownstreamCPU: Receiving Request Retry\n");
    assert(block_retry);
    // We should have a blocked packet if this function is called.
    block_retry = false;
    owner->scheduleRetryMemory();

}

void
DownstreamCPUObj::MemSidePort::attemptRetryIfNeeded()
{
    if (!block_retry && blockedPacket != nullptr) {
        // Grab the blocked packet.
        gem5::PacketPtr pkt = blockedPacket;
        blockedPacket = nullptr;

        // Try to resend it. It's possible that it fails again.
        sendPacket(pkt);
    }

}

void
DownstreamCPUObj::MemSidePort::recvRangeChange() {
    DPRINTF(DownstreamCPU, "DownstreamCPU: Receiving Range Change\n");
}

///////////////////// MEMORY ACCESS STORE

bool
DownstreamCPUObj::handleResponse(gem5::PacketPtr pkt)
{
    DPRINTF(DownstreamCPU,
        "Got response for addr %#x, %s\n", pkt->getAddr(), pkt->print());
    DDUMP(DownstreamCPU, pkt->getConstPtr<uint8_t>(), pkt->getSize());

    // The packet is now done. no need for this object to continue to stall.
    // We need to free the resource
    scheduleTick(gem5::Cycles(0));

    // Stall stop
    auto stallLen = curCycle() - stall_begin;
    if (pkt->isRead()) {
        if (cur_type == DownstreamCPUObj::MemSidePort::MemType::DRAM)
            stats.rdDRAMStalls += stallLen;
        else if (cur_type == DownstreamCPUObj::MemSidePort::MemType::SPMEM)
            stats.rdSPStalls += stallLen;
    } else if (pkt->isWrite()) {
        if (cur_type == DownstreamCPUObj::MemSidePort::MemType::DRAM)
            stats.wrDRAMStalls += stallLen;
        else if (cur_type == DownstreamCPUObj::MemSidePort::MemType::SPMEM)
            stats.wrSPStalls += stallLen;
    }

    // Finish execution of instreuction in spike
    // spike_sim->steps(1, blocked);
    delete pkt;

    return true;
}

void
DownstreamCPUObj::tick() {
        DPRINTF(DownstreamCPUTicks,
            "Downstream: Ticking main, DownstreamCPU."\
                    "for PC = %p\n", spike_sim->getPC());
        // printf("Downstream: Ticking main, DownstreamCPU."\
        //             "for PC = %p\n", spike_sim->getPC());

        scheduleTick(gem5::Cycles(1));

        spike_sim->steps(1, false);

        auto thread = dynamic_cast<gem5::SimpleThread* >(threadContexts[0]);
        thread->numInst++;
        // DPRINTF(DownstreamCPUTicks,
        //     "Downstream: Instruction. %s\n",
        //      spike_sim->disasm_cur_inst().c_str());
}


void
DownstreamCPUObj::retryMemory() {
        memPort.attemptRetryIfNeeded();
        spmemPort.attemptRetryIfNeeded();
}

void
DownstreamCPUObj::scheduleTick(gem5::Cycles num_cycles) {
    if (!tickEvent.scheduled())
        schedule(tickEvent, clockEdge(num_cycles));
}

void
DownstreamCPUObj::descheduleTick() {
    if (tickEvent.scheduled())
        deschedule(tickEvent);
}

void
DownstreamCPUObj::scheduleRetryMemory() {
    if (!retryEvent.scheduled())
        schedule(retryEvent, clockEdge(gem5::Cycles(1)));
}

void
DownstreamCPUObj::initSpike() {
    for (auto spRange : downstreamAddrRanges) {
        DPRINTF(DownstreamCPU,
            "Registering SP Mem on %X with size %lu\n",
            spRange.start(), spRange.size());
        spike_sim->registerSpMem(spRange.start(),
                                spRange.size());
    }

    for (auto memRange : dramMemAddrRanges) {
        DPRINTF(DownstreamCPU,
            "Registering Main Mem on %X with size %lu\n",
            memRange.start(), memRange.size());
        spike_sim->registerMainMem(memRange.start(),
                                memRange.size());
    }
    spike_sim->init();
    spike_sim->run();
}

// gem5::AddrRangeList
// DownstreamCPUObj::getAddrRanges() const
// {
//     DPRINTF(Downstream, "Sending new ranges\n");
//     // Just use the same ranges as whatever is on the memory side.
//     return dsAddrRange;
// }

DownstreamCPUObj::
DownstreamCPUStats::DownstreamCPUStats(gem5::statistics::Group *parent)
    : gem5::statistics::Group(parent),
      ADD_STAT(rdSPAccesses, gem5::statistics::units::Count::get(),
               "Number of read accesses to the Scratchpad Memory"),
      ADD_STAT(rdDRAMAccesses, gem5::statistics::units::Count::get(),
               "Number of read accesses to DRAM Memory"),
      ADD_STAT(wrSPAccesses, gem5::statistics::units::Count::get(),
               "Number of write accesses to Scratchpad Memory"),
      ADD_STAT(wrDRAMAccesses, gem5::statistics::units::Count::get(),
               "Number of write accesses to DRAM Memory"),
      ADD_STAT(ctrlAccess, gem5::statistics::units::Count::get(),
               "Number of accesses to Control"),
      ADD_STAT(rdSPStalls, gem5::statistics::units::Cycle::get(),
               "Number of cycles waiting for read"
               "access to Scratchpad Memory"),
      ADD_STAT(rdDRAMStalls, gem5::statistics::units::Cycle::get(),
               "Number of cycles waiting for read access to DRAM"),
      ADD_STAT(wrSPStalls, gem5::statistics::units::Cycle::get(),
               "Number of cycles waiting for write"
               "access to Scratchpad Memory"),
      ADD_STAT(wrDRAMStalls, gem5::statistics::units::Cycle::get(),
               "Number of cycles waiting for write access to DRAM")
{
}

void
DownstreamCPUObj::init() {
    BaseCPU::init();
    DPRINTF(DownstreamCPU,
            "Initializing Downstream CPU %d\n", lane_id);
    initSpike();
}

void
DownstreamCPUObj::endOfProgram(int64_t ret_code) {
    DPRINTF(DownstreamCPU,
            "Program finished with return code %d\n", ret_code);

    descheduleTick();

    for (auto & tc: threadContexts) {
        tc->halt();
        gem5::exitFunc(nullptr,tc,ret_code);
    }

}

void
DownstreamCPUObj::wakeup(gem5::ThreadID tid) {
    return;
}
gem5::Port &
DownstreamCPUObj::getDataPort() {
    return memPort;
}
gem5::Port &
DownstreamCPUObj::getInstPort() {
    return memPort;
}
gem5::Counter
DownstreamCPUObj::totalInsts() const {
    gem5::Counter numInsts=0;
    for (auto & thr: threadContexts) {
        auto thread = dynamic_cast<gem5::SimpleThread* >(thr);
        numInsts += thread->numInst++;
    }
    return numInsts;
}
gem5::Counter
DownstreamCPUObj::totalOps() const {
    // Single cycle core. So numInsts is the same as numOps
    gem5::Counter numInsts=0;
    for (auto & thr: threadContexts) {
        auto thread = dynamic_cast<gem5::SimpleThread* >(thr);
        numInsts += thread->numInst++;
    }
    return numInsts;
}