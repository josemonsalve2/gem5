#ifndef __DOWNSTREAM_OBJECT_HH__
#define __DOWNSTREAM_OBJECT_HH__

/**
 * @file downstream_obj.hh
 *
 * @author Jose M Monsalve Diaz
 * @brief Definition of a simple downstream object
 * @version 0.1
 * @date 2021-06-25
 *
 * @copyright Copyright (c) 2021
 *
 * This file is based on the simple_cache.hh example
 * in the learning_gem5 folder. The following Copyright applies:
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ======= FILE DESCRIPTION =========
 *
 * This file implements the first and simplest version of the
 * downstream object. The idea is that the CPU will send memory
 * requests to this object that stores the scratchpad memory.
 * The scratchpad memory will be allocated here, and pass to the
 * spike simulator as a memory mapped plugin. Therefore,
 * Gem5 and spike can communicate with each other.
 *
 * For now the CPU communicates to the downstream,
 * but the downstream never send messages (other than ACK)
 * to the CPU. The downstream only send messages to
 * DRAM in the form of stores (and eventually loads?)
 *
 * TODO: Questions
 * * How does the downstream know the location in DRAM of the result?
 * * It is still not clear now the clock information between
 *   spike and gem5 will work. Therefore, timing is still not working
 *
 */


#include "base/statistics.hh"
#include "mem/abstract_mem.hh" // subclass of sim_object
#include "mem/port.hh"
#include "params/DownstreamObj.hh"
#include "spike_wrapper.hh"

class DownstreamObj : public gem5::memory::AbstractMemory
{
  private:

    enum PortReq
    {
      TOP,
      DS
    };
#define REQ_NAME(req) (req == PortReq::TOP? "TOP" : "Downstream")
    /**
     * Port on the CPU-side that receives requests.
     */
    class CPUSidePort : public gem5::ResponsePort
    {
      private:
        DownstreamObj *owner;

        /// True if the port needs to send a retry req.
        bool needRetry;

        /// If we tried to send a packet and it was blocked, store it here
        gem5::PacketPtr blockedPacket;

      public:
        /**
         * Constructor. Just calls the superclass constructor.
         */
        CPUSidePort(const std::string& name, DownstreamObj *owner) :
            ResponsePort(name, owner), owner(owner), needRetry(false),
            blockedPacket(nullptr)
        { }

        /**
         * Send a packet across this port. This is called by the owner and
         * all of the flow control is hanled in this function.
         * This is a convenience function for the SimpleCache to send pkts.
         *
         * @param packet to send.
         */
        void sendPacket(gem5::PacketPtr pkt);

        /**
         * Get a list of the non-overlapping address ranges the owner is
         * responsible for. All response ports must override this function
         * and return a populated list with at least one item.
         *
         * @return a list of ranges responded to
         */
        gem5::AddrRangeList getAddrRanges() const override;

        /**
         * Send a retry to the peer port only if it is needed.
         */
        void trySendRetry();

      protected:
        /**
         * Receive an atomic request packet from the request port.
         */
        gem5::Tick recvAtomic(gem5::PacketPtr pkt) override
        { panic("recvAtomic unimpl."); }

        /**
         * Receive a functional request packet from the request port.
         *
         * @param packet the requestor sent.
         */
        void recvFunctional(gem5::PacketPtr pkt) override;

        /**
         * Receive a timing request from the request port.
         *
         * @param the packet that the requestor sent
         * @return whether this object can consume to packet. If false, we
         *         will call sendRetry() when we can try to receive this
         *         request again.
         */
        bool recvTimingReq(gem5::PacketPtr pkt) override;

        /**
         * Called by the request port if sendTimingResp was called on this
         * response port (causing recvTimingResp to be called on the request
         * port) and was unsuccessful.
         */
        void recvRespRetry() override;
    };


    virtual void init();

    /**
     * Handle the request from the CPU side. Called from the CPU port
     * on a timing request.
     *
     * @param requesting packet
     * @param id of the port to send the response
     * @return true if we can handle the request this cycle, false if the
     *         requestor needs to retry later
     */
    bool handleRequest(gem5::PacketPtr pkt);

    /**
     * Handle the respone from the memory side. Called from the memory port
     * on a timing response.
     *
     * @param responding packet
     * @return true if we can handle the response this cycle, false if the
     *         responder needs to retry later
     */
    bool handleResponse(gem5::PacketPtr pkt, DownstreamObj::PortReq requester);

    /**
     * Send the packet to the CPU side.
     * This function assumes the pkt is already a response packet and forwards
     * it to the correct port. This function also unblocks this object and
     * cleans up the whole request.
     *
     * @param the packet to send to the cpu side
     */
    void sendResponse(gem5::PacketPtr pkt, PortReq requester);

    /**
     * Handle a packet functionally. Update the data on a write and get the
     * data on a read. Called from CPU port on a recv functional.
     *
     * @param packet to functionally handle
     */
    void handleFunctional(gem5::PacketPtr pkt);

    /**
     * For timing request
     */
    void accessTiming(gem5::PacketPtr pkt, PortReq requester);

    /**
     * For timing and functional access
     *
     * @return true once the data has been "stored"
     */
    bool accessFunctional(gem5::PacketPtr pkt);


    /**
     * Tell the CPU side to ask for our memory ranges.
     */
    void sendRangeChange() const;

    /// Latency to store memory
    const gem5::Cycles top_latency;
    const gem5::Cycles ds_latency;

    /// Instantiation of the CPU-side port
    CPUSidePort cpuPort;

    /// True if this memory is currently blocked waiting for a reponse.
    bool blocked;

    /// Packet that we are currently handling.
    gem5::PacketPtr originalPacket;

    /// Map to avoid re-calculating Requestor type with string.find()
    std::map<gem5::RequestorID, PortReq> req_type;


  public:
    DownstreamObj(const gem5::DownstreamObjParams &p);

     /**
     * Get a port with a given name and index. This is used at
     * binding time and returns a reference to a protocol-agnostic
     * port.
     *
     * @param if_name Port name
     * @param idx Index in the case of a VectorPort
     *
     * @return A reference to the given port
     */
    gem5::Port &getPort(const std::string &if_name,
                  gem5::PortID idx=gem5::InvalidPortID) override;

    ~DownstreamObj() {
    }
};

#endif // __DOWNSTREAM_OBJECT_HH__
