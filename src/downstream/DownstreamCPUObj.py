
from m5.params import *
from m5.proxy import *
from m5.objects.BaseCPU import BaseCPU

class DownstreamCPUObj(BaseCPU):
    type = 'DownstreamCPUObj'
    cxx_header = "downstream/downstream_cpu_obj.hh"

    mem_side = VectorRequestPort("Main Memory side port, sends requests")
    spmem_side = \
        VectorRequestPort("Scratchpad Memory side port, sends requests")

    shared_dram_range = \
        Param.AddrRange(AddrRange(0,1), "Shared DRAM Memory Address range")

    downstream_range = \
        Param.AddrRange(AddrRange(0,1), "Shared DRAM Memory Address range")

    sync_clock = Param.Cycles(1, "Sync clock for spike")

    lane_id = Param.Int(0, "ID for the downstream lane")

    context_id = \
        Param.Int(-1, "ID for context, to not overlap with top threads")

    spikeArgs = Param.String("", "Spike simulator CLI arguments")
