
from m5.params import *
from m5.proxy import *
from m5.objects.AbstractMemory import AbstractMemory

class DownstreamObj(AbstractMemory):
    type = 'DownstreamObj'
    cxx_header = "downstream/downstream_obj.hh"
    cxx_class = 'DownstreamObj'

    cpu_side = ResponsePort("CPU side port, receives requests")

    top_latency = Param.Cycles(1, "Cycles to access memory")
    ds_latency = Param.Cycles(1, "Cycles to access memory")
