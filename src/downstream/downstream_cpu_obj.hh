#ifndef __DOWNSTREAM_OBJECT_CPU_HH__
#define __DOWNSTREAM_OBJECT_CPU_HH__

/**
 * @file downstream_obj_cpu.hh
 *
 * @author Jose M Monsalve Diaz
 * @brief Definition of a simple downstream object
 * @version 0.1
 * @date 2021-06-25
 *
 * @copyright Copyright (c) 2021
 *
 * This file is based on the simple_cache.hh example
 * in the learning_gem5 folder. The following Copyright applies:
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  ======= FILE DESCRIPTION =========
 *
 * This file implements a RISC-V base CPU using spike_wrapper.
 * Each CPU has two ports: 1) a DRAM memory port and 2) a Scratchpad memory
 * port. Multiple downstream lanes are connected via an interconnect to the
 * same scratchpad memory. And by a cross bar to access DRAM.
 */

#include "base/amo.hh"
#include "base/statistics.hh"
#include "cpu/base.hh"
#include "mem/port.hh"
#include "params/DownstreamCPUObj.hh"
#include "sim/stats.hh"
#include "sim/system.hh"
#include "spike_wrapper.hh"


class SpikeAtomicOpFunctor : public gem5::AtomicOpFunctor
{
  public:
    SpikeAtomicOpFunctor(uint8_t * _res, \
        std::function<void(uint8_t*, uint8_t*)> _op)
        :result_addr(_res), op(_op)
    {}

    void operator()(uint8_t *p) { op(result_addr, p); }

    AtomicOpFunctor* clone() override
    {
        return new SpikeAtomicOpFunctor(*this);
    }
  private:
    uint8_t * result_addr;
    std::function<void(uint8_t*, uint8_t*)> op;
 };

class DownstreamCPUObj : public gem5::BaseCPU
{
  private:
    /**
     * Port on the memory-side that receives responses.
     */
    class MemSidePort : public gem5::RequestPort
    {
      private:
        DownstreamCPUObj *owner;
        gem5::RequestorID _RequestorId;

        gem5::statistics::Scalar* rdAccessStats;
        gem5::statistics::Scalar* wrAccessStats;

        /// If we tried to send a packet and it was blocked, store it here
        gem5::PacketPtr blockedPacket;
        bool block_retry;

        public:
        enum MemType
        {
          DRAM,
          SPMEM
        };

        private:

        MemType type;

      public:
        /**
         * Constructor. Just calls the superclass constructor.
         */
        MemSidePort(const std::string& name, DownstreamCPUObj *owner,
                    gem5::statistics::Scalar* rdAccess,
                    gem5::statistics::Scalar* wrAccess, MemType _type) :
            RequestPort(name, owner), owner(owner),
            _RequestorId(owner->getRequestorID()),
            rdAccessStats(rdAccess),
            wrAccessStats(wrAccess),
            blockedPacket(nullptr), block_retry(false),
            type(_type)
        { }

        /**
         * Send a funcitonal packet across this port.
         * This is called by the owner and
         * all of the flow control is hanled in this function.
         *
         * @param packet to send.
         */
        void sendPacketFunctional(gem5::PacketPtr pkt);

        /**
         * Send a packet across this port. This is called by the owner and
         * all of the flow control is hanled in this function.
         *
         * @param packet to send.
         */
        void sendPacket(gem5::PacketPtr pkt);

        /**
         * @brief Get requestor ID
         *
         */

        gem5::RequestorID getRequestorId() { return _RequestorId; }

        MemType getType() { return type; };

        /**
         * @brief Send any pending retry packets
         *
         * In order to avoid retrying right away, which could cause deadlocks,
         * se send the packet in the next clock cycle.
         *
         * TODO: What if all the cores work at the same cycle?
         *
         */
        void attemptRetryIfNeeded();


        virtual void recvRangeChange();
      protected:
        /**
         * Receive a timing response from the response port.
         */
        bool recvTimingResp(gem5::PacketPtr pkt) override;

        /**
         * Called by the response port if sendTimingReq was called on this
         * request port (causing recvTimingReq to be called on the response
         * port) and was unsuccesful.
         */
        void recvReqRetry() override;

    };

    struct DownstreamCPUStats : public gem5::statistics::Group
    {
        DownstreamCPUStats(gem5::statistics::Group *parent);

        gem5::statistics::Scalar rdSPAccesses;
        gem5::statistics::Scalar rdDRAMAccesses;
        gem5::statistics::Scalar wrSPAccesses;
        gem5::statistics::Scalar wrDRAMAccesses;
        gem5::statistics::Scalar ctrlAccess;
        gem5::statistics::Scalar rdSPStalls;
        gem5::statistics::Scalar rdDRAMStalls;
        gem5::statistics::Scalar wrSPStalls;
        gem5::statistics::Scalar wrDRAMStalls;
    } stats;

    MemSidePort::MemType cur_type;
    gem5::Cycles stall_begin;

    virtual void init();

    /**
     * @brief Spike ends and communicates it to Gem5
     *
     * This function is sent to spike_wrapper so that it can tell Gem5 that
     * the spike program has finished. This will halt the processor and it will
     * allow the Gem5 simulation to finish.
     *
     * @param ret_code The return code of the spike program
     */
    void endOfProgram(int64_t ret_code);

    /**
     * Handle the respone from the memory side. Called from the memory port
     * on a timing response.
     *
     * @param responding packet
     * @return true if we can handle the response this cycle, false if the
     *         responder needs to retry later
     */
    bool handleResponse(gem5::PacketPtr pkt);


    /**
     * @brief This function initializes the spark simulator
     *
     * it is expected that the spike code has some runtime
     * in the form of a while (alive) loop that helps spike
     * finish execution.
     */
    void initSpike();

    /// Instantiation of the Mem-side port
    MemSidePort memPort;
    MemSidePort spmemPort;

    // Lane information
    uint32_t lane_id;
    // TODO: This should eventually dissapear in favor of
    // baseCPU + ThreadContext ?
    gem5::ContextID ctx_id;

    /// Spike simulator
    char ** spike_sim_args;
    spike::sim *spike_sim;

    gem5::Tick spikeTick;

    gem5::EventFunctionWrapper tickEvent;
    gem5::EventFunctionWrapper retryEvent;

    void tick();
    void retryMemory();
    void scheduleTick(gem5::Cycles);
    void descheduleTick();
    void scheduleRetryMemory();

    /// Premapped DRAM Memory address range
    gem5::AddrRangeList dramMemAddrRanges;
    gem5::AddrRangeList downstreamAddrRanges;

    // Base CPU methods
    void wakeup(gem5::ThreadID tid) override;
    gem5::Port &getDataPort() override;
    gem5::Port &getInstPort() override;
    gem5::Counter totalInsts() const override;
    gem5::Counter totalOps() const override;


  public:
    DownstreamCPUObj(const gem5::DownstreamCPUObjParams &p);

    gem5::AtomicOpFunctorPtr atomicOp;

    gem5::PacketPtr createReadPacket(gem5::Addr addr,
                    uint64_t size, void * result, bool atomic);
    gem5::PacketPtr createWritePacket(gem5::Addr addr,
                    uint64_t size, const void * result, bool atomic);

    MemSidePort * findPort(gem5::Addr addr);
    typedef std::unordered_map<gem5::Addr, std::function<void(void *)>> CtrlOp;
    // Moemory:
    // All control op registers are right after the end
    // of the scratchpad memory
    // address space
    // CURENT REGS:
    // CTRL_ADDR_SPACE     Get Current Cycle
    // CTRL_ADDR_SPACE+8   Reset stats
    CtrlOp ctrlOp;
    bool inline checkControl(gem5::Addr addr);
    void sendReadRequestMem(gem5::Addr addr,
                            uint64_t size,
                            void * result,
                            bool atomic);
    void sendReadRequestFuncMem(gem5::Addr addr,
                            uint64_t size,
                            void * result,
                            bool atomic);
    void sendWriteRequestMem(gem5::Addr addr,
                            uint64_t size,
                            const void * result,
                            bool atomic);
    void sendWriteRequestFuncMem(gem5::Addr addr,
                            uint64_t size,
                            const void * result,
                            bool atomic);

     /**
     * Get a port with a given name and index. This is used at
     * binding time and returns a reference to a protocol-agnostic
     * port.
     *
     * @param if_name Port name
     * @param idx Index in the case of a VectorPort
     *
     * @return A reference to the given port
     */
    gem5::Port &getPort(const std::string &if_name,
                  gem5::PortID idx=gem5::InvalidPortID) override;

    ~DownstreamCPUObj() {
      delete spike_sim_args[0];
      delete spike_sim_args;
      delete spike_sim;
      for (auto & thread : threadContexts)
        delete thread;
    }

    /**
     * @brief Get system where DS CPU is located
     *
     * Used to get the requestor ID in the port
     *
     */

    gem5::System * getSystem() { return system; }

    gem5::RequestorID getRequestorID() { return system->getRequestorId(this); }
};

#endif // __DOWNSTREAM_OBJECT_CPU_HH__
