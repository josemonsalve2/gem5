/**
 * @file downstream_obj.cc
 * @author Jose M Monsalve Diaz (jmonsalvediaz@anl.gov)
 * @brief Definition of a simple downstream object
 * @version 0.1
 * @date 2021-06-28
 *
 * @copyright Copyright (c) 2021
 *
 * This file is based on the simple_cache.cc
 * example in the learning_gem5 folder.
 * The following Copyright applies:
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */

#include "downstream/downstream_obj.hh"

#include <iostream>

#include "base/random.hh"
#include "debug/Downstream.hh"
#include "sim/system.hh"

#define SPIKE_COMMAND "spike"

DownstreamObj::DownstreamObj(const gem5::DownstreamObjParams &params) :
    AbstractMemory(params),
    top_latency(params.top_latency),
    ds_latency(params.ds_latency),
    cpuPort(params.name + ".cpu_side", this),
    blocked(false), originalPacket(nullptr)
{}


gem5::Port &
DownstreamObj::getPort(const std::string &if_name, gem5::PortID idx)
{
    // This is the name from the Python DownstreamObj
    // declaration in DownstreamObj.py
    if (if_name == "cpu_side") {
        panic_if(idx != gem5::InvalidPortID,
                 "CPU side of Downstream not a vector port");
        return cpuPort;
    } else {
        // pass it along to our super class
        return ClockedObject::getPort(if_name, idx);
    }
}

void
DownstreamObj::CPUSidePort::sendPacket(gem5::PacketPtr pkt)
{
    // If we can't send the packet across the port, store it for later.
    DPRINTF(Downstream, "Sending %s to  port\n", pkt->print());

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");

    if (!sendTimingResp(pkt)) {
        DPRINTF(Downstream, "failed!\n");
        blockedPacket = pkt;
    }
}


gem5::AddrRangeList
DownstreamObj::CPUSidePort::getAddrRanges() const
{
    gem5::AddrRangeList ranges;
    ranges.push_back(owner->getAddrRange());
    return ranges;
}


void
DownstreamObj::CPUSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(Downstream, "Sending retry req.\n");
        sendRetryReq();
    }
}


void
DownstreamObj::CPUSidePort::recvFunctional(gem5::PacketPtr pkt)
{
    // Just forward to the downstream.
    return owner->handleFunctional(pkt);
}


bool
DownstreamObj::CPUSidePort::recvTimingReq(gem5::PacketPtr pkt)
{
    DPRINTF(Downstream, "Got timing request %s\n", pkt->print());

    if (needRetry) {
        // The downstream may not be able to send a reply if this is blocked
        DPRINTF(Downstream, "CPU Request blocked\n");
        return false;
    }

    // Port is blocked, we won't be able to receive the packet,
    // but we must send a retry request in the future
    if (blockedPacket != nullptr) {
        DPRINTF(Downstream, "CPU Port is busy waiting for retry\n");
        needRetry = true;
        return false;
    }

    // Just forward to the memory.
    if (!owner->handleRequest(pkt)) {
        DPRINTF(Downstream, "Request failed\n");
        // stalling
        needRetry = true;
        return false;
    } else {
        DPRINTF(Downstream, "Request succeeded\n");
        return true;
    }
}


void
DownstreamObj::CPUSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    gem5::PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    DPRINTF(Downstream, "Retrying response pkt %s\n", pkt->print());
    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);

    // We may now be able to accept new packets
    trySendRetry();
}

bool
DownstreamObj::handleRequest(gem5::PacketPtr pkt)
{
    gem5::RequestorID rid = pkt->requestorId();

    auto requester = PortReq::TOP;
    if (req_type.find(rid) != req_type.end()) {
        requester = req_type[rid];
    } else {
        auto requestor_name = system()->getRequestorName(rid);

        if (requestor_name.find("downstream_lanes") != std::string::npos) {
            requester = PortReq::DS;
        }
        req_type[rid] = requester;
    }

    if (blocked) {
        // There is currently an outstanding request so we can't respond. Stall
        DPRINTF(Downstream, "Blocked %s!\n", REQ_NAME(requester));
        return false;
    }

    DPRINTF(Downstream, "Got request from %s for addr -- "
                "Scheduling %#x\n", REQ_NAME(requester), pkt->getAddr());

    // This downstream is now blocked waiting for the response to this packet.
    blocked = true;

    // Schedule an event after access latency to actually access
    schedule(new gem5::EventFunctionWrapper([this, pkt, requester] {
                                    accessTiming(pkt, requester); },
                                      name() + ".accessEvent", true),
             clockEdge(requester==PortReq::DS ? ds_latency : top_latency));

    return true;
}


///////////////////// MEMORY ACCESS STORE

bool
DownstreamObj::handleResponse(gem5::PacketPtr pkt,
                              DownstreamObj::PortReq requester)
{
    assert(blocked);
    DPRINTF(Downstream, "Got response from %s for addr %#x\n",\
                                REQ_NAME(requester), pkt->getAddr());

    sendResponse(pkt, requester);

    return true;
}


void DownstreamObj::sendResponse(gem5::PacketPtr pkt,
                                 DownstreamObj::PortReq requester)
{
    assert(blocked);

    DPRINTF(Downstream, "Sending resp for %s in addr %#x\n",\
                                REQ_NAME(requester), pkt->getAddr());

    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    blocked = false;

    // Simply forward to the memory port
    cpuPort.sendPacket(pkt);

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    cpuPort.trySendRetry();
}

void
DownstreamObj::handleFunctional(gem5::PacketPtr pkt)
{
    // accessFunctional() will call the Abstract memory
    // functionalAccess() method, which makes the response
    // already
    accessFunctional(pkt);
}

void
DownstreamObj::accessTiming(gem5::PacketPtr pkt,
                            DownstreamObj::PortReq requester)
{
    // accessFunctional() will call the Abstract memory
    // functionalAccess() method, which makes the response
    // already and it may change the original needResponse
    DPRINTF(Downstream, "accessTiming for packet from %s: %s\n",
            REQ_NAME(requester), pkt->print());

    bool needsResponse = pkt->needsResponse();
    accessFunctional(pkt);

    if (pkt->hasData()) {
        DDUMP(Downstream, pkt->getConstPtr<uint8_t>(), pkt->getSize());
    }

    if (needsResponse) {
        DPRINTF(Downstream, "Response needed\n");
        sendResponse(pkt, requester);
    } else {
        DPRINTF(Downstream, "No response needed\n");
        blocked = false;

        // For each of the cpu ports, if it needs to send a retry,
        // it should do it now since this memory object may be
        // unblocked now.
        cpuPort.trySendRetry();
    }
}

bool
DownstreamObj::accessFunctional(gem5::PacketPtr pkt)
{

    DPRINTF(Downstream, "Pkt is: atomic(%d), LLSC(%d),"
                        " invalidate(%d), request(%d),"
                        " ConditionalSwap(%d)\n",
                        pkt->isAtomicOp(), pkt->isLLSC(),
                        pkt->isInvalidate(), pkt->isRequest(),
                        pkt->req->isCondSwap());
    if (pkt->cmd == gem5::MemCmd::SwapReq) {
        DPRINTF(Downstream, "accessFunctional Atomic SwapReq\n");
    } else if (pkt->isWrite()) {
        // The packet should be aligned.
        DPRINTF(Downstream, "accessFunctional Write\n");
    } else if (pkt->isRead()) {
        // The packet should be aligned.
        DPRINTF(Downstream,"accessFunctional Read\n");
    } else {
        DPRINTF(Downstream, "Unknown request. Ignoring\n");
    }
    access(pkt);
    return true;
}

void
DownstreamObj::sendRangeChange() const
{
    cpuPort.sendRangeChange();
}

void
DownstreamObj::init() {
    DPRINTF(Downstream, "Initializing Downstream Memory\n");
    sendRangeChange();
}
