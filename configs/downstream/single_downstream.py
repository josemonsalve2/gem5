# -*- coding: utf-8 -*-
# Copyright (c) 2016 Jason Lowe-Power
# Copyright (c) 2020 The Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Jason Lowe-Power, Trivikram Reddy

""" Simple config/run script for the DownstreamObj

This file tests the integration with Spike through

"""

import argparse
import sys
import os

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.params import NULL
from m5.util import addToPath, fatal, warn

addToPath('../')

from ruby import Ruby

from common import Options
import Simulation
import CacheConfig
from common import CpuConfig
from common import ObjectList
import MemConfig
from common.FileSystemConfig import config_filesystem
from common.Caches import *
from common.cpu2000 import *


def create_downstream(system, configs=False):
    def_configs = {}
    def_configs["num_lanes"] = 1
    def_configs['sp_addr'] = 0x200000000
    def_configs['spmem_size'] = 0x3FFFFF
    def_configs['dram_map_addr'] = 0x90000000
    def_configs['dram_map_size'] = 0x7FFFFFFF
    def_configs['ds_latency'] = 0
    def_configs['top_latency'] = 1
    def_configs['sync_clock'] = 1
    def_configs['spike_args'] = ""
    if (not configs):
        configs = def_configs
    else:
        for key, val in def_configs.items():
            if (key not in configs):
                configs[key] = val

    def printConfig():
        print("configs:")
        for c,v in configs.items():
            if type(v) is int:
                print(f"\t{c}: {hex(v)}")
            else:
                print(f"\t{c}: {v}")

    printConfig()
    dsAddrRange = AddrRange(start=Addr(configs['sp_addr']),
                            end=Addr(configs['sp_addr']) +
                            configs['spmem_size'])
    mapAddrRange = AddrRange(start=Addr(configs['dram_map_addr']),
                            end=Addr(configs['dram_map_addr']) +
                            configs['dram_map_size'])
    system.downstream_spmem = DownstreamObj(
                                top_latency=configs['top_latency'],
                                ds_latency=configs['ds_latency'],
                                range=dsAddrRange)
    np = len(system.cpu)

    xbar = CoherentXBar(width=32, \
                        snoop_filter=SnoopFilter(lookup_latency = 0))
    xbar.badaddr_responder = BadAddr()
    xbar.default = Self.badaddr_responder.pio
    xbar.width = 64
    xbar.frontend_latency = 0
    xbar.forward_latency = 0
    xbar.response_latency = 0
    xbar.header_latency = 0
    xbar.snoop_response_latency = 0
    system.downstream_spmem.downstream_xbar = xbar

    system.downstream_spmem.bridge = Bridge(ranges = dsAddrRange, delay='0ps')

    system.downstream_spmem.downstream_xbar.mem_side_ports = \
                        system.downstream_spmem.bridge.cpu_side_port
    system.downstream_spmem.bridge.mem_side_port = \
                        system.downstream_spmem.cpu_side

    for i in range(np):
        system.downstream_spmem.downstream_xbar.cpu_side_ports = \
                                        system.cpu[i].l1xbar.mem_side_ports
    # for r in system.mem_ranges:
    #     print(str(r.start)+" - "+str(r.end))
    system.downstream_spmem.downstream_lanes = [ DownstreamCPUObj(
                                    sync_clock=configs["sync_clock"],
                                    shared_dram_range=mapAddrRange,
                                    downstream_range=dsAddrRange,
                                    spikeArgs=configs["spike_args"],
                                    lane_id = lane,
                                    context_id=len(system.cpu) + lane)
                                    for lane in range(configs["num_lanes"])]
    for idx, lane in enumerate(system.downstream_spmem.downstream_lanes):
        lane.spmem_side = \
            system.downstream_spmem.downstream_xbar.cpu_side_ports
        lane.mem_side = system.membus.slave
        lane.createThreads()
        lane.createInterruptController()
        process = Process(pid = len(system.cpu) + idx)
        process.cmd = [configs['top_program']]
        lane.workload = process


def get_processes(args):
    """Interprets provided args and returns a list of processes"""

    multiprocesses = []
    inputs = []
    outputs = []
    errouts = []
    pargs = []

    workloads = args.cmd.split(';')
    if args.input != "":
        inputs = args.input.split(';')
    if args.output != "":
        outputs = args.output.split(';')
    if args.errout != "":
        errouts = args.errout.split(';')
    if args.options != "":
        pargs = args.options.split(';')
    print(workloads)
    idx = 0
    for wrkld in workloads:
        process = Process(pid = 100 + idx)
        process.executable = wrkld
        process.cwd = os.getcwd()

        if args.env:
            with open(args.env, 'r') as f:
                process.env = [line.rstrip() for line in f]

        if len(pargs) > idx:
            process.cmd = [wrkld] + pargs[idx].split()
        else:
            process.cmd = [wrkld]

        if len(inputs) > idx:
            process.input = inputs[idx]
        if len(outputs) > idx:
            process.output = outputs[idx]
        if len(errouts) > idx:
            process.errout = errouts[idx]

        multiprocesses.append(process)
        idx += 1

    if args.smt:
        assert(args.cpu_type == "DerivO3CPU")
        return multiprocesses, idx
    else:
        return multiprocesses, 1


parser = argparse.ArgumentParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

parser.add_argument("--spike-program", action="store",
                        type=str, dest="spike",
                        help="spike program to execute",
                        default="/opt/riscv/riscv64-unknown-elf/bin/pk b.o")
parser.add_argument("--ds-spmem-addr", action="store",
                        type=str, dest="ds_spmem_addr",
                        help="Start address DS scratchpad memory",
                        default="0x200000000")
parser.add_argument("--ds-spmem-size", action="store",
                        type=str, dest="ds_spmem_size",
                        help="Size of downstream scratchpad memory",
                        default="0x3FFFFF")
parser.add_argument("--map-mem-addr", action="store",
                        type=str, dest="map_mem_addr",
                        help="Start address of pre-mapped region",
                        default="0x90000000")
parser.add_argument("--map-mem-size", action="store",
                        type=str, dest="map_mem_size",
                        help="Size of pre-mapped region",
                        default="0x3FFFFF")
parser.add_argument("--ds-num-lanes", action="store",
                        type=int, dest="ds_num_lanes",
                        help="Number of lanes in Downstream",
                        default=2)

parser.add_argument("--dramsim3-ini", type=str,
                help="dramsim3 ini config file")

if '--ruby' in sys.argv:
    Ruby.define_options(parser)

args = parser.parse_args()

multiprocesses = []
numThreads = 1

if args.bench:
    apps = args.bench.split("-")
    if len(apps) != args.num_cpus:
        print("number of benchmarks not equal to set num_cpus!")
        sys.exit(1)

    for app in apps:
        try:
            if buildEnv['TARGET_ISA'] == 'arm':
                exec("workload = %s('arm_%s', 'linux', '%s')" % (
                        app, args.arm_iset, args.spec_input))
            else:
                exec("workload = %s(buildEnv['TARGET_ISA', 'linux', '%s')" % (
                        app, args.spec_input))
            multiprocesses.append(workload.makeProcess())
        except:
            print("Unable to find workload for %s: %s" %
                  (buildEnv['TARGET_ISA'], app),
                  file=sys.stderr)
            sys.exit(1)
elif args.cmd:
    multiprocesses, numThreads = get_processes(args)
else:
    print("No workload specified. Exiting!\n", file=sys.stderr)
    sys.exit(1)


(CPUClass, test_mem_mode, FutureClass) = Simulation.setCPUClass(args)
CPUClass.numThreads = numThreads

# Check -- do not allow SMT with multiple CPUs
if args.smt and args.num_cpus > 1:
    fatal("You cannot use SMT with multiple CPUs!")

np = args.num_cpus
mp0_path = multiprocesses[0].executable
system = System(cpu = [CPUClass(cpu_id=i) for i in range(np)],
                mem_mode = test_mem_mode,
                mem_ranges = [AddrRange(args.mem_size)],
                cache_line_size = args.cacheline_size)

if numThreads > 1:
    system.multi_thread = True

# Create a top-level voltage domain
system.voltage_domain = VoltageDomain(voltage = args.sys_voltage)

# Create a source clock for the system and set the clock period
system.clk_domain = SrcClockDomain(clock =  args.sys_clock,
                                   voltage_domain = system.voltage_domain)

# Create a CPU voltage domain
system.cpu_voltage_domain = VoltageDomain()

# Create a separate clock domain for the CPUs
system.cpu_clk_domain = SrcClockDomain(clock = args.cpu_clock,
                                       voltage_domain =
                                       system.cpu_voltage_domain)

# If elastic tracing is enabled, then configure the cpu and attach the elastic
# trace probe
if args.elastic_trace_en:
    CpuConfig.config_etrace(CPUClass, system.cpu, args)

# All cpus belong to a common cpu_clk_domain, therefore running at a common
# frequency.
for cpu in system.cpu:
    cpu.clk_domain = system.cpu_clk_domain

if ObjectList.is_kvm_cpu(CPUClass) or ObjectList.is_kvm_cpu(FutureClass):
    if buildEnv['TARGET_ISA'] == 'x86':
        system.kvm_vm = KvmVM()
        for process in multiprocesses:
            process.useArchPT = True
            process.kvmInSE = True
    else:
        fatal("KvmCPU can only be used in SE mode with x86")

# Sanity check
if args.simpoint_profile:
    if not ObjectList.is_noncaching_cpu(CPUClass):
        fatal("SimPoint/BPProbe should be done with an atomic cpu")
    if np > 1:
        fatal("SimPoint generation not supported with more than one CPUs")

for i in range(np):
    if args.smt:
        system.cpu[i].workload = multiprocesses
    elif len(multiprocesses) == 1:
        system.cpu[i].workload = multiprocesses[0]
    else:
        system.cpu[i].workload = multiprocesses[i]

    if args.simpoint_profile:
        system.cpu[i].addSimPointProbe(args.simpoint_interval)

    if args.checker:
        system.cpu[i].addCheckerCpu()

    if args.bp_type:
        bpClass = ObjectList.bp_list.get(args.bp_type)
        system.cpu[i].branchPred = bpClass()

    if args.indirect_bp_type:
        indirectBPClass = \
            ObjectList.indirect_bp_list.get(args.indirect_bp_type)
        system.cpu[i].branchPred.indirectBranchPred = indirectBPClass()

    system.cpu[i].createThreads()

if args.ruby:
    Ruby.create_system(args, False, system)
    assert(args.num_cpus == len(system.ruby._cpu_ports))

    system.ruby.clk_domain = SrcClockDomain(clock = args.ruby_clock,
                                        voltage_domain = system.voltage_domain)
    for i in range(np):
        ruby_port = system.ruby._cpu_ports[i]

        # Create the interrupt controller and connect its ports to Ruby
        # Note that the interrupt controller is always present but only
        # in x86 does it have message ports that need to be connected
        system.cpu[i].createInterruptController()

        # Connect the cpu's cache ports to Ruby
        ruby_port.connectCpuPorts(system.cpu[i])
else:
    MemClass = Simulation.setMemClass(args)
    system.membus = SystemXBar()
    system.system_port = system.membus.slave
    CacheConfig.config_cache(args, system)
    MemConfig.config_mem(args, system)
    config_filesystem(system, args)

system.workload = SEWorkload.init_compatible(mp0_path)

if args.wait_gdb:
    system.workload.wait_for_remote_gdb = True

root = Root(full_system = False, system = system)

# Create an instantiation of the simobject
# you created EndOfDRAM - EndOfDRAM + DSMemorySize
ds_configs = {}

def hex_or_dec(val):
    if "0x" in val:
        return int(val, 16)
    else:
        return int(val)

ds_configs["spmem_addr"] = hex_or_dec(args.ds_spmem_addr)
ds_configs["spmem_size"] = hex_or_dec(args.ds_spmem_size)
ds_configs["map_mem_addr"] = hex_or_dec(args.map_mem_addr)
ds_configs["map_mem_size"] = hex_or_dec(args.map_mem_size)

ds_configs["spike_args"] = args.spike
ds_configs["top_program"] = args.cmd
ds_configs["num_lanes"] = args.ds_num_lanes

create_downstream(system, ds_configs)

Simulation.initialize(args, root, system, FutureClass)
## Mapping Downstream memory to the process
for process in multiprocesses:
    process.map(Addr(ds_configs["spmem_addr"]),
                Addr(ds_configs["spmem_addr"]),
                ds_configs["spmem_size"],
                False)
    # Mapping pre-mapped DRAM region for
    # DRAM communication between Up/Down and Top
    process.map(Addr(ds_configs["map_mem_addr"]),
                Addr(ds_configs["map_mem_addr"]),
                ds_configs["map_mem_size"],
                False)

Simulation.run(args, root, system, FutureClass)



