# -*- coding: utf-8 -*-
# Copyright (c) 2016 Jason Lowe-Power
# Copyright (c) 2020 The Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Jason Lowe-Power, Trivikram Reddy

import argparse
import sys
import os
# from gem5.configs.common import CacheConfig

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.params import NULL
from m5.util import addToPath, fatal, warn

addToPath('../')

from system_skylake.core import *
from system_skylake.caches import *

from common import Options
from common import CpuConfig
from common import ObjectList
import Simulation
import CacheConfig
import MemConfig
from common.FileSystemConfig import config_filesystem

### DOWNSTREAM CONFIG
def create_downstream(system, configs=False):
    def_configs = {}
    def_configs["num_lanes"] = 1
    def_configs['sp_addr'] = 0x200000000
    def_configs['spmem_size'] = 0x3FFFFF
    def_configs['dram_map_addr'] = 0x90000000
    def_configs['dram_map_size'] = 0x7FFFFFFF
    def_configs['ds_latency'] = 0
    def_configs['top_latency'] = 1
    def_configs['sync_clock'] = 1
    def_configs['spike_args'] = ""
    if (not configs):
        configs = def_configs
    else:
        for key, val in def_configs.items():
            if (key not in configs):
                configs[key] = val

    def printConfig():
        print("configs:")
        for c,v in configs.items():
            if type(v) is int:
                print(f"\t{c}: {hex(v)}")
            else:
                print(f"\t{c}: {v}")

    printConfig()
    dsAddrRange = AddrRange(start=Addr(configs['sp_addr']),
                            end=Addr(configs['sp_addr']) +
                            configs['spmem_size'])
    mapAddrRange = AddrRange(start=Addr(configs['dram_map_addr']),
                            end=Addr(configs['dram_map_addr']) +
                            configs['dram_map_size'])
    system.downstream_spmem = DownstreamObj(
                                top_latency=configs['top_latency'],
                                ds_latency=configs['ds_latency'],
                                range=dsAddrRange)
    np = len(system.cpu)

    xbar = CoherentXBar(width=32, \
                        snoop_filter=SnoopFilter(lookup_latency = 0))
    xbar.badaddr_responder = BadAddr()
    xbar.default = Self.badaddr_responder.pio
    xbar.width = 64
    xbar.frontend_latency = 0
    xbar.forward_latency = 0
    xbar.response_latency = 0
    xbar.header_latency = 0
    xbar.snoop_response_latency = 0
    system.downstream_spmem.downstream_xbar = xbar

    system.downstream_spmem.bridge = Bridge(ranges = dsAddrRange, delay='0ps')

    system.downstream_spmem.downstream_xbar.mem_side_ports = \
                        system.downstream_spmem.bridge.cpu_side_port
    system.downstream_spmem.bridge.mem_side_port = \
                        system.downstream_spmem.cpu_side

    for i in range(np):
        system.downstream_spmem.downstream_xbar.cpu_side_ports = \
                                        system.cpu[i].l1xbar.mem_side_ports
    # for r in system.mem_ranges:
    #     print(str(r.start)+" - "+str(r.end))
    system.downstream_spmem.downstream_lanes = [DownstreamCPUObj(
                                    sync_clock=configs["sync_clock"],
                                    shared_dram_range=mapAddrRange,
                                    downstream_range=dsAddrRange,
                                    spikeArgs=configs["spike_args"],
                                    lane_id = lane,
                                    context_id=len(system.cpu) + lane)
                                    for lane in range(configs["num_lanes"])]
    for idx, lane in enumerate(system.downstream_spmem.downstream_lanes):
        lane.spmem_side = \
            system.downstream_spmem.downstream_xbar.cpu_side_ports
        lane.mem_side = system.membus.slave
        lane.createThreads()
        # This is needed because Gem5 forces us to use an x86 baseCPU.
        # The ISA cannot be changed for different cores. We execute RISCV
        # inside Spike, so we never really execute any X86 program inside
        lane.createInterruptController()
        lane.interrupts[0].pio = system.membus.mem_side_ports
        lane.interrupts[0].int_requestor = system.membus.cpu_side_ports
        lane.interrupts[0].int_responder = system.membus.mem_side_ports
        process = Process(pid = len(system.cpu) + idx)
        process.cmd = [configs['top_program']]

        lane.workload = process

def get_processes(args):
    """Interprets provided args and returns a list of processes"""

    multiprocesses = []
    inputs = []
    outputs = []
    errouts = []
    pargs = []

    workloads = args.cmd.split(';')
    if args.input != "":
        inputs = args.input.split(';')
    if args.output != "":
        outputs = args.output.split(';')
    if args.errout != "":
        errouts = args.errout.split(';')
    if args.options != "":
        pargs = args.options.split(';')
    print(workloads)
    idx = 0
    for wrkld in workloads:
        process = Process(pid = 100 + idx)
        process.executable = wrkld
        process.cwd = os.getcwd()

        if args.env:
            with open(args.env, 'r') as f:
                process.env = [line.rstrip() for line in f]

        if len(pargs) > idx:
            process.cmd = [wrkld] + pargs[idx].split()
        else:
            process.cmd = [wrkld]

        if len(inputs) > idx:
            process.input = inputs[idx]
        if len(outputs) > idx:
            process.output = outputs[idx]
        if len(errouts) > idx:
            process.errout = errouts[idx]

        multiprocesses.append(process)
        idx += 1

    if args.smt:
        assert(args.cpu_type == "DerivO3CPU")
        return multiprocesses, idx
    else:
        return multiprocesses, 1

class SkylakeSystem(System):
    _CPUModel = BaseCPU
    def __init__(self, args):
        super().__init__()
        self._CPUClass = None
        self._multiprocesses = []
        self._FutureClass = None
        numThreads = 1

        if args.cmd:
            self._multiprocesses, numThreads = get_processes(args)
        else:
            print("No workload specified. Exiting!\n", file=sys.stderr)
            sys.exit(1)

        (self._CPUClass, test_mem_mode, self._FutureClass) = \
                                            Simulation.setCPUClass(args)
        self._CPUClass.numThreads = numThreads

        np = args.num_cpus

        # Check -- do not allow SMT with multiple CPUs
        if args.smt and args.num_cpus > 1:
            fatal("You cannot use SMT with multiple CPUs!")

        # Use skylake CPUs
        if args.skylake_cpu == "VerbatimCPU":
            self._CPUClass = VerbatimCPU
            self._FutureClass = VerbatimCPU
        elif args.skylake_cpu == "TunedCPU":
            self._CPUClass = TunedCPU
            self._FutureClass = TunedCPU
        elif args.skylake.cpu == "UnconstrainedCPU":
            self._CPUClass = UnconstrainedCPU
            self._FutureClass = UnconstrainedCPU


        self.cpu = [self._CPUClass(cpu_id=i) for i in range(np)]

        # self.clk_domain = SrcClockDomain()
        # self.clk_domain.clock = '3.5GHz'
        # self.clk_domain.voltage_domain = VoltageDomain()

        self.mem_mode = test_mem_mode
        # Create a top-level voltage domain
        self.voltage_domain = VoltageDomain(voltage = args.sys_voltage)

        # Create a source clock for the system and set the clock period
        self.clk_domain = SrcClockDomain(clock =  args.sys_clock,
                                        voltage_domain = self.voltage_domain)

        # Create a CPU voltage domain
        self.cpu_voltage_domain = VoltageDomain()

        # Create a separate clock domain for the CPUs
        self.cpu_clk_domain = SrcClockDomain(clock = args.cpu_clock,
                                            voltage_domain =
                                            self.cpu_voltage_domain)
        #mem_size = '8GB'
        self.mem_ranges = [AddrRange(args.mem_size)]

        # If elastic tracing is enabled, then configure
        # the cpu and attach the elastic trace probe
        if args.elastic_trace_en:
            CpuConfig.config_etrace(self._CPUClass, self.cpu, args)

        # All cpus belong to a common cpu_clk_domain,
        #  therefore running at a common frequency.
        for cpu in self.cpu:
            cpu.clk_domain = self.cpu_clk_domain

        for i in range(np):
            if args.smt:
                self.cpu[i].workload = self._multiprocesses
            elif len(self._multiprocesses) == 1:
                self.cpu[i].workload = self._multiprocesses[0]
            else:
                self.cpu[i].workload = self._multiprocesses[i]

            if args.simpoint_profile:
                self.cpu[i].addSimPointProbe(args.simpoint_interval)

            if args.checker:
                self.cpu[i].addCheckerCpu()

            if args.bp_type:
                bpClass = ObjectList.bp_list.get(args.bp_type)
                self.cpu[i].branchPred = bpClass()

            if args.indirect_bp_type:
                indirectBPClass = \
                    ObjectList.indirect_bp_list.get(args.indirect_bp_type)
                self.cpu[i].branchPred.indirectBranchPred = indirectBPClass()

            self.cpu[i].createThreads()

        # Create a memory bus
        MemClass = Simulation.setMemClass(args)
        self.membus = SystemXBar(width = 192)
        self.membus.badaddr_responder = BadAddr()
        self.membus.default = Self.badaddr_responder.pio

        # Set up the system port for functional access from the simulator
        self.system_port = self.membus.cpu_side_ports

        # Create L1
        for cpu in self.cpu:
            cpu.l1xbar = CoherentXBar(width=192,
                    snoop_filter=SnoopFilter(lookup_latency = 0))
            cpu.l1xbar.badaddr_responder = BadAddr()
            cpu.l1xbar.default = Self.badaddr_responder.pio
            cpu.l1xbar.frontend_latency = 0
            cpu.l1xbar.forward_latency = 0
            cpu.l1xbar.response_latency = 0
            cpu.l1xbar.snoop_response_latency = 0

            # Create an L1 instruction and data cache
            icache = L1ICache()
            icache.addr_ranges = self.mem_ranges
            dcache = L1DCache()
            dcache.addr_ranges = self.mem_ranges
            iwalkcache = MMUCache()
            dwalkcache = MMUCache()
            # When connecting the caches, the clock is also inherited
            # from the CPU in question
            CacheConfig.addPrivateSplitL1Caches(cpu,
                                    icache, dcache,
                                    iwalkcache, dwalkcache,
                                    cpu.l1xbar)

        # Create a memory bus, a coherent crossbar, in this case
        self.l3bus = L2XBar(width = 192,
                            snoop_filter = SnoopFilter(max_capacity='32MB'))

        # Create L2
        for cpu in self.cpu:
            cpu.l2bus = L2XBar(width = 192)
            # Hook the CPU ports up to the l2bus
            cpu.icache.connectBus(cpu.l2bus)
            cpu.dcache.connectBus(cpu.l2bus)
            cpu.itb_walker_cache.connectBus(cpu.l2bus)
            cpu.dtb_walker_cache.connectBus(cpu.l2bus)

            # Create an L2 cache and connect it to the l2bus
            cpu.l2cache = L2Cache()
            cpu.l2cache.connectCPUSideBus(cpu.l2bus)

            # Connect the L2 cache to the l3bus
            cpu.l2cache.connectMemSideBus(self.l3bus)

        # Create an L3 cache and connect it to the l3bus
        self.l3cache = L3Cache()
        self.l3cache.connectCPUSideBus(self.l3bus)

        # Connect the L3 cache to the membus
        self.l3cache.connectMemSideBus(self.membus)

        # create the interrupt controller for the CPU
        for cpu in self.cpu:
            cpu.createInterruptController()
            cpu.interrupts[0].pio = self.membus.mem_side_ports
            cpu.interrupts[0].int_requestor = self.membus.cpu_side_ports
            cpu.interrupts[0].int_responder = self.membus.mem_side_ports


        # provide cache paramters for verbatim CPU
        if (self._CPUModel is VerbatimCPU):
            for cpu in self.cpu:
                # L1I-Cache
                cpu.icache.size = '32kB'
                cpu.icache.tag_latency = 4
                cpu.icache.data_latency = 4
                cpu.icache.response_latency = 1
                # L1D-Cache
                cpu.dcache.tag_latency = 4
                cpu.dcache.data_latency = 4
                cpu.dcache.response_latency = 1

        MemConfig.config_mem(args, self)
        config_filesystem(self, args)


parser = argparse.ArgumentParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

parser.add_argument("--spike-program", action="store",
                        type=str, dest="spike",
                        help="spike program to execute",
                        default="/opt/riscv/riscv64-unknown-elf/bin/pk b.o")
parser.add_argument("--ds-spmem-addr", action="store",
                        type=str, dest="ds_spmem_addr",
                        help="Start address DS scratchpad memory",
                        default="0x200000000")
parser.add_argument("--ds-spmem-size", action="store",
                        type=str, dest="ds_spmem_size",
                        help="Size of downstream scratchpad memory",
                        default="0x3FFFFF")
parser.add_argument("--map-mem-addr", action="store",
                        type=str, dest="map_mem_addr",
                        help="Start address of pre-mapped region",
                        default="0x90000000")
parser.add_argument("--map-mem-size", action="store",
                        type=str, dest="map_mem_size",
                        help="Size of pre-mapped region",
                        default="0x3FFFFF")
parser.add_argument("--ds-num-lanes", action="store",
                        type=int, dest="ds_num_lanes",
                        help="Number of lanes in Downstream",
                        default=2)

parser.add_argument("--skylake-cpu", action="store",
                        dest="skylake_cpu",default=None,
                        choices=['VerbatimCPU',
                                 'TunedCPU',
                                 'UnconstrainedCPU'],
                        help="Select which class of CPU to use with the"
                             "skylake configuration. These are all derived"
                             "from Deriv03CPU")

parser.add_argument("--dramsim3-ini", type=str,
                help="dramsim3 ini config file")

args = parser.parse_args()

system = SkylakeSystem(args)

if not args.skylake_cpu:
    print("If you want to use skylake CPU, use --skylake-cpu=[class] "
            "where class is any of these options " +
            str(['VerbatimCPU', 'TunedCPU', 'UnconstrainedCPU']))

mp0_path = system._multiprocesses[0].executable

system.workload = SEWorkload.init_compatible(mp0_path)

if args.wait_gdb:
    system.workload.wait_for_remote_gdb = True

root = Root(full_system = False, system = system)

# Create an instantiation of the simobject
# you created EndOfDRAM - EndOfDRAM + DSMemorySize
ds_configs = {}

def hex_or_dec(val):
    if "0x" in val:
        return int(val, 16)
    else:
        return int(val)

ds_configs["spmem_addr"] = hex_or_dec(args.ds_spmem_addr)
ds_configs["spmem_size"] = hex_or_dec(args.ds_spmem_size)
ds_configs["map_mem_addr"] = hex_or_dec(args.map_mem_addr)
ds_configs["map_mem_size"] = hex_or_dec(args.map_mem_size)

ds_configs["spike_args"] = args.spike
ds_configs["top_program"] = args.cmd
ds_configs["num_lanes"] = args.ds_num_lanes

create_downstream(system, ds_configs)

Simulation.initialize(args, root, system, system._FutureClass)
## Mapping Downstream memory to the process
for process in system._multiprocesses:
    process.map(Addr(ds_configs["spmem_addr"]),
                Addr(ds_configs["spmem_addr"]),
                ds_configs["spmem_size"],
                False)

    # Mapping pre-mapped DRAM region for
    # DRAM communication between Up/Down and Top
    process.map(Addr(ds_configs["map_mem_addr"]),
                Addr(ds_configs["map_mem_addr"]),
                ds_configs["map_mem_size"],
                False)

Simulation.run(args, root, system, system._FutureClass)